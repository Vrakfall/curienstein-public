:PROPERTIES:
:ID:       ee6fbe3b-07ce-402c-92f6-b10952d452b2
:END:
#+title: Zen (Browser)
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-25 Sun 15:01]
#+filetags: :resource:public:

- is :: [[id:9ca1eb6c-0cf4-4465-b599-5a42ad38dba3][Open Source]] [[id:0b03e9ec-5abb-4b46-b469-f176c9cbb580][Browser]]
- fork of :: [[id:ba16e94d-a79b-448a-a6e3-0c434ec1d35d][Firefox]]

* [[id:138275c8-3869-4cf0-950d-e0c1ef43ab24][Privacy]]
- It sure looks not as private as [[id:af129084-35b3-4975-a424-161e986a0f70][Librewolf]].
  - source :: [[https://github.com/zen-browser/desktop/issues/475][Missing `privacy.resistFingerprinting.letterboxing` Option · Issue #475 · zen-browser/desktop · GitHub]]
* References
- [[https://www.zen-browser.app/][Website]]
- [[https://github.com/zen-browser/desktop][Repository]]
