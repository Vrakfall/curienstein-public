:PROPERTIES:
:ID:       d714bf8c-57ad-4c41-b912-e4a61c925797
:END:
#+title: Nextcloud
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-10-10 jeu 00:14]
#+filetags: :resource:public:

- is :: [[id:fc022085-f233-4d37-8264-939a018f7089][Client-Server Software]] [[id:66aaa116-d50f-435c-9253-836f93770684][File Synchronisation]] [[id:ec963401-4886-4be1-9189-33cdb797f796][File Hosting]] [[id:dcd43817-e630-4e43-bd97-62ce421c33ec][Office Suite]] [[id:3cf46768-ff01-4bff-b90d-8a3680ccafe2][Home Cloud Platform]] [[id:403c2ee9-8f4e-4a84-88fa-54a96dae22c9][Decentralized]] [[id:9ca1eb6c-0cf4-4465-b599-5a42ad38dba3][Open Source]]
- license :: [[id:8e3d60e1-bf09-4cc2-bfaf-4a51a2bc2cd7][AGPL v3]]
- languages :: [[id:674b41b5-15ed-4ada-a087-552cc7fa761d][PHP]] [[id:b10f8536-d710-48f4-abf3-831fba795df2][JavaScript]] [[id:1e950542-9e2e-47a4-8bd1-653ab308e308][Python]] [[id:885f79de-4b0f-457b-9d44-c1ddf6a4463f][Shell]] [[id:c6c21ca5-da43-4b74-bd6f-0f467a850205][Vue.js]]

* [[id:5c165c6e-8afc-4514-974d-c989ff433860][Extending]] [[id:1d6ae52d-4b95-424b-8e3f-65af1195f8f5][Applications]]
- [[id:d9ce8a4d-699b-4786-a303-1f925be2ca0a][Cospend]]
* References
- [[https://nextcloud.com/][Website]]
- [[https://github.com/nextcloud][Repositories]]
- [[https://en.wikipedia.org/wiki/Nextcloud][Wikipedia]]
- [[https://apps.nextcloud.com/][App Store]]
** [[id:e95553b7-d4a2-413d-8ddc-58d4f5c84b75][Documentation]]
- [[https://docs.nextcloud.com/server/latest/user_manual/en/][User Manual]]
- [[https://docs.nextcloud.com/server/latest/admin_manual/][Admin Manual]]
- [[https://docs.nextcloud.com/server/latest/developer_manual/][Developer Manual]]
