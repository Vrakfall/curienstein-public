:PROPERTIES:
:ID:       46e2edd4-a5d0-49ba-836f-1cd83ab23724
:ROAM_ALIASES: centralized
:END:
#+title: Centralized
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-01-26 ven 21:38]
#+filetags: :resource:public:

- as opposed to :: [[id:403c2ee9-8f4e-4a84-88fa-54a96dae22c9][Decentralized]]
