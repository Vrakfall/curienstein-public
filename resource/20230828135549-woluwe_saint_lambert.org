:PROPERTIES:
:ID:       d978e947-740b-4267-a118-c4ce569f0859
:END:
#+title: Woluwe-Saint-Lambert
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-08-28 ma 13:55]
#+filetags: :resource:public:

- is :: [[id:ed6a361d-9f16-4e5e-aca2-020a994b162f][Town]] [[id:9f893448-e46d-4cbf-8ee7-c00503021a4a][City]]
- location :: [[id:858c8987-3af9-4dd3-b801-1fa633d00612][Belgium]] [[id:6927f3e2-1d89-47f6-b2e7-d7701f2e5a1a][Brussels]]
