:PROPERTIES:
:ID:       18b8e42c-5eb1-4d16-ac75-8c3b7e85ae47
:ROAM_ALIASES: "Secure Sockets Layer" SSL "Transport Layer Security"
:END:
#+title: TLS
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-04 Tue 22:33]

- is :: [[id:b9707425-7063-47c6-a426-91d1817ab595][Protocol]] [[id:159dd5f3-06c3-4a6e-8e30-f9da14de2478][Cryptography]] [[id:88664cce-c3cb-4986-81e9-52468f7b1177][Encryption]]

* References
- [[https://en.wikipedia.org/wiki/Transport_Layer_Security][Wikipedia]]
