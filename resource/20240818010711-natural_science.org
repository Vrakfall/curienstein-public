:PROPERTIES:
:ID:       0300efc6-7b87-47d1-95e3-7a064bad294b
:ROAM_ALIASES: "natural sciences" "natural science" "Natural Sciences"
:END:
#+title: Natural Science
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-18 dim 01:07]
#+filetags: :resource:public:

- is :: [[id:9f8bfd74-843c-4126-ba99-fc55bee6a837][Science]]

* References
- https://en.wikipedia.org/wiki/Natural_science
