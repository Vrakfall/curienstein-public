:PROPERTIES:
:ID:       bdbbb4e0-cf89-4090-a668-358cdbc92f5f
:ROAM_ALIASES: "programming language idioms" "programming language idiom" "Programming Language Idioms"
:END:
#+title: Programming Language Idiom
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-03-19 mar 22:48]
#+filetags: :resource:public:

- is :: [[id:70a3b408-c4f6-46a7-af76-2db6372c7e79][Programming Language]] [[id:8629b792-db18-4b59-b747-38217beac092][Idiom]]
