:PROPERTIES:
:ID:       bc4cf6a2-ca9b-43eb-9e97-35f2450ca726
:ROAM_ALIASES: VPSs VPS
:END:
#+title: Virtual Private Server
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-10-12 sam 05:19]
#+filetags: :resource:public:

- is :: [[id:16feca8a-211b-4a73-8f88-1494c076901d][Virtual]] [[id:87ca9ec0-6c9a-40f6-a553-e93c9476d556][Server]]

* References
- [[https://en.wikipedia.org/wiki/Virtual_private_server][Wikipedia]]
