:PROPERTIES:
:ID:       fe454195-5b00-43b8-8245-9a0d58400049
:ROAM_ALIASES: "package building"
:END:
#+title: Package Building
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-05-18 Thu 11:00]
#+filetags: :resource:public:

- is :: [[id:ba5c6977-113a-44e8-b4ad-b48423e8054c][Package]] [[id:f951198f-b3f3-44bb-8a8e-bb4fe61e301f][Building]]
- relates to :: [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]]
