:PROPERTIES:
:ID:       2f701117-dc7d-4e7e-ad56-9aa31d3543b5
:END:
#+title: Alistair Young
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-06 Thu 12:54]

- is :: [[id:54e1ce63-1614-413b-893d-067bd4b16db0][Person]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:0aaab563-259c-4b78-971e-dc5874230600][Developer]]

* References
- [[https://github.com/cerebrate][GitHub]]
- [[https://arkane-systems.net/][Website]]
- [[https://github.com/arkane-systems?q=linux&type=all&language=&sort=][GitHub sub-project -- Arkane Systems]]
