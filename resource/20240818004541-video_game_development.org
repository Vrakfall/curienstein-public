:PROPERTIES:
:ID:       44653112-0a19-4198-a4b9-4e579cb63f89
:ROAM_ALIASES: "video game development"
:END:
#+title: Video Game Development
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-18 dim 00:45]
#+filetags: :resource:public:

- is ::  [[id:7c2a15ee-0a9f-4c73-9151-bf70f553fde1][Video Game]] [[id:bd9bf76b-9d33-4b81-a5aa-8db0d3d3c5b9][Development]]

* [[id:a02f52f7-fee6-4bd1-bc15-e2294b8e5904][Design]] Aspects
** [[id:285b8816-d6d4-4cc8-a55a-792bbbe177b3][Networking]]
- [[https://github.com/bevyengine/bevy/discussions/8675][What kind of networking should X game use? · bevyengine/bevy · Discussion #8675 · GitHub]]
* References
- [[https://en.wikipedia.org/wiki/Video_game_development][Wikipedia]]
