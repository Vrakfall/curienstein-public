:PROPERTIES:
:ID:       9d3ca17c-4509-406f-be6e-f54f6764dde1
:ROAM_ALIASES: CARP
:END:
#+title: Common Address Redundancy Protocol
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-10-17 jeu 01:18]
#+filetags: :resource:public:

- is :: [[id:285b8816-d6d4-4cc8-a55a-792bbbe177b3][Network]] [[id:9fa3649c-ca35-424e-b527-b4f5b02fcf70][Router]] [[id:b9707425-7063-47c6-a426-91d1817ab595][Protocol]]
- authors :: [[id:232ccc80-1dfe-426e-b161-d091fc500ae1][OpenBSD]]

* [[id:3f046046-a35e-4e47-9ae7-2ec4b89e4395][Example]] [[id:f802081a-ec05-419a-8dbb-2c572eb1cc5a][implementations]]
- [[https://www.kernel-panic.it/openbsd/carp/][Redundant firewalls with OpenBSD, CARP and pfsync - Table of contents]]
* References
- [[https://en.wikipedia.org/wiki/Common_Address_Redundancy_Protocol][Wikipedia]]
- [[https://man.openbsd.org/carp][Manpage]]
- [[https://www.openbsd.org/faq/pf/carp.html][FAQ/Documentation]]
