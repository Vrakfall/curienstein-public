:PROPERTIES:
:ID:       ebca8518-3f4a-441a-9c0a-7bb11e270a88
:ROAM_ALIASES: dkms DKMS
:END:
#+title: Dynamic Kernel Module Support
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-10-03 jeu 00:51]
#+filetags: :resource:public:

- is :: [[id:bbf78978-13bf-40a2-967c-2883325de8c2][Linux Kernel]] [[id:9defc6ff-d727-4c6f-9acf-c8c07022406a][Framework]]
- purpose :: [[id:f9ef2b46-4e55-42ab-bc2d-2bc76eaa3e1b][Loading]]

* Description
* References
- [[https://wiki.archlinux.org/title/Dynamic_Kernel_Module_Support][ArchWiki]]
