:PROPERTIES:
:ID:       ef3a5b20-24bf-4f6a-b9da-cc80822e4fd9
:END:
#+title: Distribution (Network)
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-08-10 do 15:32]
#+filetags: :resource:public:

- is :: [[id:b2473354-9c0e-4543-b4a4-723bd205b6ad][Distribution]]
- relates to :: [[id:285b8816-d6d4-4cc8-a55a-792bbbe177b3][Network]]

* Description
In [[id:285b8816-d6d4-4cc8-a55a-792bbbe177b3][Network]] [[id:a02f52f7-fee6-4bd1-bc15-e2294b8e5904][Design]], the act of linking the [[id:4303d7a0-c4ab-4712-a628-2fa3eeb3f6d1][End-User]] to the [[id:d4b9db66-defe-4d16-8ff8-d48de99691d6][Backbone]].
