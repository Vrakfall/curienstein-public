:PROPERTIES:
:ID:       42428978-c49d-44c6-81ee-0375cfbee8c1
:END:
#+title: QEMU
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-10-11 ven 01:45]
#+filetags: :resource:public:

- is :: [[id:59d0e01c-2bf5-4290-9bb6-b3e07ff2328c][Generic]] [[id:9ca1eb6c-0cf4-4465-b599-5a42ad38dba3][Open Source]] [[id:311d8508-7319-4251-a085-6e0ee1a8e5d3][Machine Emulator]] [[id:aa52f394-8352-4bf6-ac5c-b914a26206ef][Virtualizer]]

* [[id:1897c834-3aaa-4fda-8d7b-b9bbca5d4bd8][Tools]]
- [[id:456606a3-98a1-4819-9fcb-9f4834eaca69][Quickemu]]
- [[id:e903c909-f120-463c-aff5-c0efe2d00296][QEMU helper scripts]]
* References
- [[https://www.qemu.org/][Website]]
