:PROPERTIES:
:ID:       28c483e0-2660-4e59-b749-59dc72c5a495
:ROAM_ALIASES: "Cisco Licenses"
:END:
#+title: Cisco License
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-11-14 jeu 17:55]
#+filetags: :resource:public:

- is :: [[id:59bdf795-fa4b-4071-8a66-c2a2f0d5fd1e][Cisco]] [[id:956ca970-c141-4e4a-b56b-ca3b61ef4596][Proprietary License]]

* Activation
- For now, check [[id:2a2aceb7-3096-4432-83b9-e82afbfe9085][Cisco License activation]] in [[id:be73ef1a-c855-402c-a130-a285bb6ef0ea][Cisco 1921 Series Integrated Services Router]].
* Places to buy
** Untested
- [[https://itprice.com/cisco-gpl/security k9][SECURITY K9 Price - Cisco Global Price List]]
* References
- [[https://www.cisco.com/c/en/us/td/docs/routers/access/sw_activation/SA_on_ISR.html][Software Activation on Cisco Integrated Services Routers - Cisco]]
