:PROPERTIES:
:ID:       021ff05a-00c0-4b23-bd64-67855bcbb2cd
:END:
#+title: Zaventem
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-11-24 vr 10:06]
#+filetags: :resource:public:

- is :: [[id:ed6a361d-9f16-4e5e-aca2-020a994b162f][Town]] [[id:9f893448-e46d-4cbf-8ee7-c00503021a4a][City]]
- location :: [[id:858c8987-3af9-4dd3-b801-1fa633d00612][Belgium]] [[id:6927f3e2-1d89-47f6-b2e7-d7701f2e5a1a][Brussels]]
