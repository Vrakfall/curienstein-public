:PROPERTIES:
:ID:       096e56fa-7f53-4930-a844-bd37d126918c
:ROAM_ALIASES: "to be discovered" "To Be Discovered" "to discover" "To Discover" "to explore" "To Explore" "to be explored"
:END:
#+title: To Be Explored
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-03-25 Sat 12:25]

- variation of :: [[id:c1080e4b-18ad-4b15-b27f-a9b051c2fc62][To Try]]
- nuance ::
  - requires :: [[id:4def3296-9f9b-421d-897b-579fcf49c127][Pertinence Check]]
