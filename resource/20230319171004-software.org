:PROPERTIES:
:ID:       40af0fb6-55e9-42f1-959b-3a53ad16abf5
:ROAM_ALIASES: software
:END:
#+title: Software
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-03-19 Sun 17:10]

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]]
- relates to :: [[id:9350d7cb-f0b8-43cb-bbe1-8a1e25a50872][Programs]]
