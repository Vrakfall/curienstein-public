:PROPERTIES:
:ID:       b567aade-bbc7-483a-b8fe-0767c312b541
:ROAM_ALIASES: seasons season Seasons
:END:
#+title: Season
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-02-02 vr 08:43]
#+filetags: :resource:public:

- relates to :: [[id:1820a567-4cb0-44a0-94bc-b697a1820b02][Time]] of the [[id:96ef045d-369e-4ce5-ada8-25538fe7dc2a][Year]]
