:PROPERTIES:
:ID:       03bd49d3-acec-4782-953c-5334bb8f28eb
:END:
#+title: su
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-09-11 mer 02:54]
#+filetags: :resource:public:

- is :: [[id:89e95cfe-f8bf-4a1b-bb38-ad3335d25a9e][Command]]
- purpose :: [[id:2224e9bd-acbe-4f75-aba5-7523b53deec0][Elevated Privileges]]
- like :: [[id:6d056664-d59b-48d8-8a73-2390d91dceca][sudo]]
