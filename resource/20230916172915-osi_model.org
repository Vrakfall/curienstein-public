:PROPERTIES:
:ID:       f22ba5fc-f26f-476a-aeee-fd36352ecb73
:ROAM_ALIASES: OSI
:END:
#+title: OSI Model
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-09-16 sam 17:29]
#+filetags: :resource:public:

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:285b8816-d6d4-4cc8-a55a-792bbbe177b3][Network]]

* References
- [[https://en.wikipedia.org/wiki/OSI_model][Wikipedia]]
