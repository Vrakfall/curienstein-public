:PROPERTIES:
:ID:       4aee9c6f-c490-4a3e-9c6e-0637180ab110
:ROAM_ALIASES: jails jail Jails Jail
:END:
#+title: Jail (FreeBSD)
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-30 Fri 12:53]
#+filetags: :resource:public:

- is :: [[id:970f1690-2f8a-48b7-8d4a-3cc5bc45e89e][FreeBSD]] [[id:9e2d8bf9-a751-475a-bfc9-c6cae5e97a3a][Containerization]] [[id:be523ad1-3f71-43cf-a0fc-fb4278018fda][System]]

* Usage
- source :: [[https://docs.freebsd.org/en/books/handbook/jails/][Chapter 17. Jails and Containers | FreeBSD Documentation Portal]]
** [[id:0c1753c8-b94f-47db-97c6-28a2c812eef9][List]] [[id:4aee9c6f-c490-4a3e-9c6e-0637180ab110][jails]]
#+begin_src shell
jls
#+end_src
** Start [[id:4aee9c6f-c490-4a3e-9c6e-0637180ab110][jail]]
#+begin_src shell
service jail start jailname
#+end_src
** Stop [[id:4aee9c6f-c490-4a3e-9c6e-0637180ab110][jail]]
#+begin_src shell
service jail stop jailname
#+end_src
** Install [[id:ba5c6977-113a-44e8-b4ad-b48423e8054c][packages]] from the host [[id:be523ad1-3f71-43cf-a0fc-fb4278018fda][system]]
#+begin_src shell
pkg -j jailname install packagename
#+end_src
* References
- [[https://docs.freebsd.org/en/books/handbook/jails/][Handbook Entry]]
- [[https://wiki.freebsd.org/Jails][Wiki Entry]]
