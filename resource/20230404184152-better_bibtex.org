:PROPERTIES:
:ID:       0713d248-f630-4986-a8ba-0cc7557cbbeb
:ROAM_ALIASES: BBT
:END:
#+title: Better BibTeX
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-04 Tue 18:41]

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:5c165c6e-8afc-4514-974d-c989ff433860][Extension]]
- extends :: [[id:486cb21f-2f76-4512-a375-f60eff55c00e][Zotero]]

* Description
** From me
Better and [[id:2479147a-4d3c-44ea-97dc-18b207b69796][automatic]] generation of [[id:8bea10b0-c4b7-4483-8a3e-34f863c5c843][citation]] keys than [[id:486cb21f-2f76-4512-a375-f60eff55c00e][Zotero]]'s built-in export.
* References
- [[https://retorque.re/zotero-better-bibtex/][Documentation/Manual]]
- [[https://github.com/retorquere/zotero-better-bibtex/releases/latest][Downloads]]
- [[https://github.com/retorquere/zotero-better-bibtex/][Repository]]
