:PROPERTIES:
:ID:       18d3cec7-6384-4297-9766-e49308b00569
:END:
#+title: KWin
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-09 Sun 23:27]
#+filetags: :public:resource:

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:6eb3bc84-18fc-4e7d-a097-d679d440293a][Window Manager]]
- desktop environment :: [[id:91c7a0a4-476c-4be3-9932-9e8d9b9e5419][KDE]]
- works on :: [[id:8d078d92-e03e-4335-94e2-18610c0ef9e6][Xorg]] [[id:f0f14423-bbb3-42d0-98b0-82088b179bf4][Wayland]]

* [[id:b8d9cf29-9732-4604-b1de-2c4dea220901][Virtual Desktop]]
** [[id:356e35ea-349f-453f-b01b-6549d36367a9][Independent]] [[id:b8d9cf29-9732-4604-b1de-2c4dea220901][Virtual Desktop]] per [[id:c0d04a53-69b5-448a-9e66-3625758dbe19][screen]]
This is currently impossible as of [[id:5a82a832-c832-4f00-9cba-21c685d5507f][Plasma]] version =6.1.4=.
- source ::
  - [[https://bugs.kde.org/show_bug.cgi?id=107302][Official Issue/Ticket]]
  - [[https://discuss.kde.org/t/bug-fix-per-screen-virtual-desktops/16241/][Pledges' thread]]
  - Other posts talking about it
    - [[https://old.reddit.com/r/kde/comments/lijbee/multi_monitor_independent_desktop_workspace/][Multi Monitor Independent Desktop Workspace Switching? : kde]]
    - [[https://discuss.kde.org/t/virtual-desktop-on-primary-screen-only/11753][Virtual desktop on primary screen only - Help - KDE Discuss]]
