:PROPERTIES:
:ID:       d103a0bf-4aab-4608-a438-20616d2b8694
:END:
#+title: Deurne
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-08-10 do 13:42]
#+filetags: :resource:public:

- is :: [[id:ed6a361d-9f16-4e5e-aca2-020a994b162f][Town]]
- location :: [[id:858c8987-3af9-4dd3-b801-1fa633d00612][Belgium]]
