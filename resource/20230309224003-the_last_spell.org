:PROPERTIES:
:ID:       20c04c8b-09d8-415e-a217-c2f6b95bc88d
:END:
#+title: The Last Spell
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-03-09 Thu 22:40]

- is :: [[id:7c2a15ee-0a9f-4c73-9151-bf70f553fde1][Video Game]] [[id:a90d1109-a5fe-4af6-9687-3a046b943b09][Top-Down]] [[id:e593ef95-34ac-4157-a336-3d5266c5d76a][2D]] [[id:caae0dbe-f3cc-4bc3-bffd-1690392dbdc6][Turn-Based]] [[id:bd31ba28-7b12-437b-bfdf-3a9a28027f09][Tactical]] [[id:b2c9c5e7-963a-4f0f-be3b-6761e4e5f962][Base Building]] [[id:dd9efd33-d4ba-4808-b61b-0055bf41c1c5][RPG]] [[id:de7e8972-2483-4d21-b506-4451f574ed41][Roguelite]] [[id:395741ab-f707-494e-bf87-36d0664e0c84][Dark]] [[id:cd551812-0fad-4382-a271-3bdda68292f1][Fantasy]] [[id:0ac9826f-e05d-43ad-be72-4e4f03599be7][Post-Apocalyptic]]
- modes :: [[id:08f6a4e8-af4b-4e9c-b306-eba947f0d04d][Single-Player]]
- music :: [[id:c94eefdd-272b-4841-9e4a-561bd2c29fde][The Algorithm]]
- engine :: [[id:a8af4755-d950-4a82-9bd0-a6d057a8cbf3][Unity]]
- platforms :: [[id:e6fc9b9c-2699-4667-9463-2435c32a327a][Windows]] [[id:501f3085-3ba5-42e1-843c-0af4b5feec58][Playstation 4]] [[id:9e29163a-8a3b-4121-b071-2fbf1a6bd311][Playstation 5]] [[id:7caec973-97bc-4b90-bbb5-bad907feb190][Switch]]
- release :: <2023-03-09 Thu>
- has :: [[id:b5168654-736c-466b-9543-2e6a0a1d6f4e][Good Music]]
- playing status :: [[id:c1080e4b-18ad-4b15-b27f-a9b051c2fc62][To Try]]
- possession status :: [[id:631854be-2fbe-48a3-a043-46b4bb0fbee3][Not Possessed]]
- found from :: [[id:b20fc7f3-1fc1-490a-9521-4f97881998f8][Bahroo]]

* References
- [[https://store.steampowered.com/app/1105670/The_Last_Spell/][Steam]]
- [[https://en.wikipedia.org/wiki/The_Last_Spell][Wikipedia]]
- [[https://lastspell.com/][Website]]
- [[https://www.pcgamingwiki.com/wiki/The_Last_Spell][PCGamingWiki]]
- [[https://thelastspell.fandom.com/wiki/The_Last_Spell_Wiki][Fandom Wiki]]
