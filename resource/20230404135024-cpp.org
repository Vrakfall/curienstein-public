:PROPERTIES:
:ID:       7648642a-6c4e-4675-8600-4b501f8feede
:END:
#+title: C++
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-04 Tue 13:50]

- is :: [[id:70a3b408-c4f6-46a7-af76-2db6372c7e79][Programming Language]] [[id:26ccd8ba-acc9-4b46-9abb-c180de566455][Low-Level]]
- family :: [[id:7e847e10-87c9-4c8c-809d-8d84427f61a5][C]]

* References
- [[https://en.wikipedia.org/wiki/C%2B%2B][Wikipedia]]
