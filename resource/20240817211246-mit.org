:PROPERTIES:
:ID:       c2024a69-e310-46c0-b911-c5fa9fab5423
:END:
#+title: MIT
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-17 sam 21:12]
#+filetags: :resource:public:

- is :: [[id:0a57021a-d408-4f2e-897e-8bb670276a72][Open Source License]] [[id:19349e8f-65a8-4d56-8d3c-2520bd1fa0dd][Permissive (Licensing)]]

* References
- [[https://mit-license.org/][Website]]
- [[https://en.wikipedia.org/wiki/MIT_License][Wikipedia]]
- [[https://opensource.org/license/mit][The MIT License – Open Source Initiative]]
