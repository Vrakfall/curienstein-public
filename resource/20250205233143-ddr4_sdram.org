:PROPERTIES:
:ID:       482c6fca-8d67-4c40-91c9-9b36d13d2bee
:END:
#+title: DDR4 SDRAM
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2025-02-05 mer 23:31]
#+filetags: :resource:public:

- is :: [[id:f8ee34dd-53da-461f-aed7-8c75f1fe9f31][Synchronous Dynamic Random-Access Memory]]

* [[id:4959f84f-51c4-4e26-bd6d-2379115c0e60][Standards]]
- DDR4-1600 (PC4-12800)
- DDR4-1866 (PC4-14900)
- DDR4-2133 (PC4-17000)
- DDR4-2400 (PC4-19200)
- DDR4-2666 (PC4-21300)
- DDR4-2933 (PC4-23466)
- DDR4-3200 (PC4-25600)
* References
- [[https://en.wikipedia.org/wiki/DDR4_SDRAM][Wikipedia]]
