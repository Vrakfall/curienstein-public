:PROPERTIES:
:ID:       80e9fbe5-e9bf-48b5-8d36-0764e889f7e0
:ROAM_ALIASES: stories story Stories
:END:
#+title: Story
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-07-02 Tue 13:06]
#+filetags: :resource:public:

- relates to :: [[id:bcfd0188-e43f-4388-bc78-536f767684aa][Storytelling]]
