:PROPERTIES:
:ID:       78307294-9370-4f5b-979b-233209c0aae2
:ROAM_REFS: https://www.youtube.com/watch?v=qlfm3MEbqYA
:END:
#+title: Is NixOS The Best Gaming Distro | Linux Gaming Setup
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-24 Sat 16:17]
#+filetags: :reference-note:public:

- is :: [[id:e1838f55-9960-4d55-9808-db5eb2ae9552][Reference Note]]
- of :: [[id:c03481f0-b5de-470b-95b2-635cd3dd0988][Youtube]] [[id:a2c584f6-827f-449c-8462-9d816917d8e6][Video]]
- subject :: [[id:7c2a15ee-0a9f-4c73-9151-bf70f553fde1][Video Game]] on [[id:972b16f3-19fd-482d-a48d-206b52370df2][NixOS]]
- author :: [[id:bc88bdd4-dde8-49fe-a6b7-f895bb55fb7b][Vimjoyer]]
- quotes :: [[id:43537dba-4199-4572-bcbf-99489125d25b][Steam]] [[id:ef6cf89b-d12e-4b1e-aeef-3c4c0ea80a94][Lutris]]

* [[id:3650e767-5227-4474-bcf9-a89b671635b3][Notes]]
Mentions a lot of tricks to make playing [[id:7c2a15ee-0a9f-4c73-9151-bf70f553fde1][video games]] better on [[id:f5d42fbb-35de-4b1d-9ce1-adc5c8bd179d][Linux]] and [[id:972b16f3-19fd-482d-a48d-206b52370df2][NixOS]].
- [[id:51dd1469-5432-427d-810a-41471c657662][To Research]]
* References
- [[https://www.youtube.com/watch?v=qlfm3MEbqYA][Youtube]]
- [[https://inv.nadeko.net/watch?v=qlfm3MEbqYA][Invidious]]
