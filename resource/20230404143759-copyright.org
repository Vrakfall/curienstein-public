:PROPERTIES:
:ID:       33353e98-6dd5-49dd-9d3e-55e16874810e
:ROAM_ALIASES: copyright
:END:
#+title: Copyright
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-04 Tue 14:37]

- is :: [[id:3acfa401-c4bb-434c-821b-cc506b718df3][Non-Open License]]

* References
- [[https://en.wikipedia.org/wiki/Copyright][Wikipedia]]
