:PROPERTIES:
:ID:       a6b924c6-35b3-48a5-bd44-d349733a72ee
:END:
#+title: OpenMandriva
#+filetags: :missing-details:
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-06 Thu 12:29]

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:8d41a5d4-dd88-41ff-8eff-c32ca49bad7e][Operating System]] [[id:f5d42fbb-35de-4b1d-9ce1-adc5c8bd179d][Linux]] [[id:b2473354-9c0e-4543-b4a4-723bd205b6ad][Distribution]] [[id:9ca1eb6c-0cf4-4465-b599-5a42ad38dba3][Open Source]]

* References
- [[https://www.openmandriva.org/][Website]]
- [[https://wiki.openmandriva.org/en/home][Wiki]]
