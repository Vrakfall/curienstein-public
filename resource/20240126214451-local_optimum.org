:PROPERTIES:
:ID:       da7e5da6-5689-4301-a667-e0b5dd2ee83b
:ROAM_ALIASES: "local optimums" "local optimum" "Local Optimums"
:END:
#+title: Local Optimum
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-01-26 ven 21:44]
#+filetags: :resource:public:

- is :: [[id:3890ec44-5ce3-4cf6-952a-b8117f9972ed][Uni-variate Optimization]]

* References
- [[https://www.geeksforgeeks.org/local-and-global-optimum-in-uni-variate-optimization/][Local and Global Optimum in Uni-variate Optimization - GeeksforGeeks]]
