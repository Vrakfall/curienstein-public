:PROPERTIES:
:ID:       64924874-2950-4ab2-a5a9-1ad1f43897b4
:END:
#+title: Doom Emacs
#+filetags: :public:resource:
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-03-16 Thu 13:46]

- is :: [[id:1e6bf6c7-2048-43d0-986f-22c84d008e6c][Emacs]] [[id:9ca1eb6c-0cf4-4465-b599-5a42ad38dba3][Open Source]] [[id:b2473354-9c0e-4543-b4a4-723bd205b6ad][Distribution]]

* Description
** From me
Doom is a distribution of [[id:1e6bf6c7-2048-43d0-986f-22c84d008e6c][Emacs]].
* [[id:635a1c86-40de-43c3-b03b-1e241d33aef9][Configuration]]
** [[id:db75e854-a57a-4a46-8a7c-5c60107bb64b][Defaults]]
- Autoloaded
  - [[https://github.com/doomemacs/doomemacs/tree/master/modules/config/default][doomemacs/modules/config/default at master · doomemacs/doomemacs · GitHub]]
- [[id:6b1a9664-6a75-4e80-8a3e-3037c4ea037f][User]] [[id:635a1c86-40de-43c3-b03b-1e241d33aef9][configuration]] [[id:e395dc3a-4a77-4f10-8f18-70abf85ab70f][file]] [[id:01f168d3-1563-44e9-b8a1-0391ef6177ea][templates]]
  - [[https://github.com/doomemacs/doomemacs/tree/master/templates][doomemacs/templates at master · doomemacs/doomemacs · GitHub]]
** [[id:0818490a-1d5d-4729-9bbf-e41b43460315][Key]] bindgins
*** Layer [[id:ba5c6977-113a-44e8-b4ad-b48423e8054c][package]]
**** [[id:196fce36-ea92-4788-818a-9f0d016245a6][Evil]]
*** Guides
- [[https://discourse.doomemacs.org/t/how-to-re-bind-keys/56][How to (re)bind keys - Guides & Tutorials / Configuration - Doom Emacs Discourse]]
- [[https://discourse.doomemacs.org/t/what-are-leader-and-localleader-keys/153][What are <leader> and <localleader> keys? - Guides & Tutorials - Doom Emacs Discourse]]
*** Examples
**** Doom defaults
- [[https://github.com/doomemacs/doomemacs/blob/master/modules/config/default/+evil-bindings.el][doomemacs/+evil-bindings.el at master · doomemacs/doomemacs · GitHub]]
** Caveats
*** =:after= doesn't always work the way we expect it to
- [[https://discourse.doomemacs.org/t/difference-between-after-and-after/3489/][Difference between after! and :after - User Support - Doom Emacs Discourse]]
- [[https://github.com/jwiegley/use-package/issues/829][`:after` does not respect `after-load-alist` insertion order · Issue #829 · jwiegley/use-package · GitHub]]
** Other [[id:54e1ce63-1614-413b-893d-067bd4b16db0][People]]'s [[id:635a1c86-40de-43c3-b03b-1e241d33aef9][configurations]]
- [[https://zzamboni.org/post/my-doom-emacs-configuration-with-commentary/][zzamboni.org | My Doom Emacs configuration, with commentary]]
- [[id:c2fcd394-e74e-4297-ae6a-679126cf69e5][tecosaur]]'s [[id:635a1c86-40de-43c3-b03b-1e241d33aef9][configuration]]
- [[https://hieuphay.com/doom-emacs-config/][My Doom Emacs configurations · Hieu Phay]]
- [[https://ruivieira.dev/doom-emacs][DOOM Emacs · Rui Vieira]]
- [[https://notes.justin.vc/config][Doom Emacs Configuration]]
* References
- [[https://github.com/doomemacs/doomemacs][Website]]
- [[https://discourse.doomemacs.org/][Discourse]]
** Documentation
- [[id:4c97e83e-9471-4f2f-b928-881bf241c0d8][Org files]]
- [[id:d126a271-ceb5-4833-b976-acdd661da3a7][Module Org files]]
- [[id:5fa8967a-532f-4e0c-8ae8-25cd802bf9a9][Frequently Asked Questions]]
