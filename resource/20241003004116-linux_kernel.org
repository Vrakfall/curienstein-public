:PROPERTIES:
:ID:       bbf78978-13bf-40a2-967c-2883325de8c2
:ROAM_ALIASES: "Linux kernels" "Linux kernel" "Linux Kernels"
:END:
#+title: Linux Kernel
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-10-03 jeu 00:41]
#+filetags: :resource:public:

- is :: [[id:f5d42fbb-35de-4b1d-9ce1-adc5c8bd179d][Linux]] [[id:be0ab119-1786-4958-932f-458352fa7121][Kernel]]

* [[id:7e9fdccb-203f-423f-8807-287b74b7609b][Books]]
- [[https://0xax.gitbooks.io/linux-insides/content/][Introduction · Linux Inside]]
* References
