:PROPERTIES:
:ID:       e3474cbc-4a98-4ef8-910c-887b3cc13a5d
:ROAM_ALIASES: "session key exchange"
:END:
#+title: Session Key Exchange
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-09-16 sam 19:00]
#+filetags: :resource:public:

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:159dd5f3-06c3-4a6e-8e30-f9da14de2478][Cryptography]]
- relates to :: [[id:c4514d43-e0c1-45b7-94de-40a593419409][Session Key]]
