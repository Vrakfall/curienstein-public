:PROPERTIES:
:ID:       e04d83cd-663f-4b1a-8e2f-8200f69b5984
:ROAM_ALIASES: "configuration files" "configuration file" "Configuration Files"
:END:
#+title: Configuration File
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-02-25 dim 19:06]
#+filetags: :resource:public:

- is :: [[id:e395dc3a-4a77-4f10-8f18-70abf85ab70f][File]]
- goal :: [[id:635a1c86-40de-43c3-b03b-1e241d33aef9][Configuration]]
