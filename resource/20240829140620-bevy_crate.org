:PROPERTIES:
:ID:       0c2c442d-05dd-4911-b84d-fbcc814978b1
:ROAM_ALIASES: "bevy crate" "bevy crates" "Bevy crates" "Bevy crate" "Bevy Crates"
:END:
#+title: Bevy Crate
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-29 jeu 14:06]
#+filetags: :resource:public:

- is :: [[id:f3b43b2d-21a2-4334-a78e-46fa2c5506bd][Rust Crate]]
- specialized for :: [[id:3301c32f-b988-448e-83fd-901af9355c96][Bevy]]

* Preamble
Don't forget to check the backlinks to see all the sorted ones!
* [[id:0c1753c8-b94f-47db-97c6-28a2c812eef9][List]] of unsorted [[id:0c2c442d-05dd-4911-b84d-fbcc814978b1][Bevy crates]] that could be useful in the future
- [[https://github.com/Jondolf/bevy_transform_interpolation][GitHub - Jondolf/bevy_transform_interpolation: A general-purpose transfom interpolation plugin for fixed timesteps for the Bevy game engine.]]
- [[https://github.com/tigregalis/bevy_djqf][GitHub - tigregalis/bevy_djqf]]
- [[https://github.com/TotalKrill/bevy_mod_reqwest][GitHub - TotalKrill/bevy_mod_reqwest]]
- [[https://github.com/dioxuslabs/taffy][GitHub - DioxusLabs/taffy: A high performance rust-powered UI layout library]]
