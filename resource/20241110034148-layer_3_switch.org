:PROPERTIES:
:ID:       8298a58e-66d0-42a4-9903-f055478d960e
:ROAM_ALIASES: "layer 3 switches" "layer 3 switch" "Layer 3 Switches"
:END:
#+title: Layer 3 Switch
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-11-10 dim 03:41]
#+filetags: :resource:public:

- is :: [[id:9fa3649c-ca35-424e-b527-b4f5b02fcf70][Router]] [[id:06260983-0d82-4747-a1ed-93cd30d89342][Inter-Vlan]] [[id:160ed614-4079-4f5e-8b04-88f3f2f9345e][Switch]]

* References
