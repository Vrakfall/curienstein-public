:PROPERTIES:
:ID:       b654be37-ca37-4558-ad1e-d7731de8c503
:ROAM_ALIASES: ore
:END:
#+title: Ore
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-07-02 mar 14:15]
#+filetags: :resource:public:

- is :: [[id:597c09dc-8f5e-46d9-8b9f-5bdc216f712f][Mineral]] [[id:b6dce3e5-84bc-468c-b5b8-3e07c380a9a2][Rock]] [[id:976d9ac1-5db0-4bff-a221-49bd3922be0d][Material]]
