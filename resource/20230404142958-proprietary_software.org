:PROPERTIES:
:ID:       7d8b9a71-d938-4294-be4e-dee001d12ead
:ROAM_ALIASES: "proprietary Software"
:END:
#+title: Proprietary Software
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-04 Tue 14:29]

- is :: [[id:956ca970-c141-4e4a-b56b-ca3b61ef4596][Proprietary License]]

* References
- [[https://en.wikipedia.org/wiki/Proprietary_software][Wikipedia]]
