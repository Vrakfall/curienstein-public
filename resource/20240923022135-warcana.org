:PROPERTIES:
:ID:       f8595752-70da-4e30-bbf3-eaef2bc18df1
:END:
#+title: WARCANA
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-09-23 lun 02:21]
#+filetags: :resource:public:

- is :: [[id:7c2a15ee-0a9f-4c73-9151-bf70f553fde1][Video Game]] [[id:e9632538-6b26-44ef-8773-018574660479][Interesting]] [[id:096e56fa-7f53-4930-a844-bd37d126918c][To Be Discovered]]

* References
- [[https://store.steampowered.com/app/2022930/WARCANA/][Steam]]
