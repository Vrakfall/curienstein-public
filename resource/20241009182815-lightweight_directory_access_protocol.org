:PROPERTIES:
:ID:       0e985789-f595-4691-9689-a14c9c7a5fb6
:ROAM_ALIASES: ldap LDAP
:END:
#+title: Lightweight Directory Access Protocol
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-10-09 mer 18:28]
#+filetags: :resource:public:

- is :: [[id:285b8816-d6d4-4cc8-a55a-792bbbe177b3][Network]] [[id:b9707425-7063-47c6-a426-91d1817ab595][Protocol]] [[id:1e664ced-fa80-4ef9-acd2-941b4cc301c7][Directory Service]]

* Description
* References
- [[https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol][Wikipedia]]
