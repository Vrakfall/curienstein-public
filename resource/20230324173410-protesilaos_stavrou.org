:PROPERTIES:
:ID:       972d2ffc-8c0f-4316-83ff-a354cfbc3f7a
:ROAM_ALIASES: Prot
:END:
#+title: Protesilaos Stavrou
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-03-24 Fri 17:34]

- is :: [[id:54e1ce63-1614-413b-893d-067bd4b16db0][Person]]
- name in [[id:4fdc9171-ef7a-423d-b65b-9cb1b6d1dc9b][Greek]] :: Πρωτεσίλαος
- birth place :: [[id:349ac0bd-430e-407a-94c1-f0c65c91c267][Greece]]
- birth year :: 1988
- occupation :: [[id:9d3b99c9-12e3-4c49-bb3d-5e1f725d9696][Coach]]/Tutor on [[id:1e6bf6c7-2048-43d0-986f-22c84d008e6c][Emacs]], [[id:f5d42fbb-35de-4b1d-9ce1-adc5c8bd179d][Linux]] and [[id:bf897eeb-b2b2-4873-a73e-042aa46e3d28][Life]]
- email :: public@protesilaos.com
- interests :: [[id:1e6bf6c7-2048-43d0-986f-22c84d008e6c][Emacs]] [[id:eff4c865-2b7a-4275-a8b1-0fe591553acb][Emacs package]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:bd9bf76b-9d33-4b81-a5aa-8db0d3d3c5b9][Development]] [[id:9ca1eb6c-0cf4-4465-b599-5a42ad38dba3][Open Source]] [[id:49226236-d10f-495f-85b1-98c0bfc8395c][Poetry]] [[id:32b12e9f-6cde-4194-9916-dcd97a0dc0d3][Politics]] [[id:1690d06b-7a3c-44c7-94e6-892249084965][European Union]] [[id:f5d42fbb-35de-4b1d-9ce1-adc5c8bd179d][Linux]] [[id:bf897eeb-b2b2-4873-a73e-042aa46e3d28][Life]] [[id:c805b67f-0ac6-4d4b-adae-1a728bde784a][Philosophy]]

* [[id:156734c8-a010-4cc5-aa24-6ce8c0fe0927][PGP]] public [[id:0818490a-1d5d-4729-9bbf-e41b43460315][key]]
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQENBFkyQvkBCADmjpu9/KnKErtaIfmKs1xuM7aSmF+DJYzult2MQDdsea+pWGyi
c2puFBS4Iq6Y+DjN18xryooQppmctKciwyIZ4H23Q62W6aWC+/fdnPw0OJex4cKe
Wn9IZ7HZrOZOXFOWG6a5pf+TeS4lH3FtWw5MsHpvHtW74Kqmm3i4DgtDP2TYWw41
9nKIUNPFCvr/CAwYgB2cQQxdkhxC4c5xbbs1uFNgQNkzY9nOL1c5ypaH6J/lokMa
EWhZTbY0AzFw5TyHIzt36qEkMPKOj+iSZ547dLSEyCu6JrMsdFlE0eb1mihMMVTI
gOcVswrjcSZN40zwDkYK+J6xKX9gHTZYCaGdABEBAAG0LFByb3Rlc2lsYW9zIFN0
YXZyb3UgPHB1YmxpY0Bwcm90ZXNpbGFvcy5jb20+iQFUBBMBCAA+AhsDBQsJCAcC
BhUICQoLAgQWAgMBAh4BAheAFiEEpbihbYat7LIfuNSIv0j9hP4h1yoFAmQX5OgF
CQ6oCO8ACgkQv0j9hP4h1ypGZwf8CcOnK/eTISJiKdpRxUDnfUy0jItpJtYT6uDY
Vn++JLxqKkWl1tR2YTbVStxgkATiE6/Oy6W8SPtjwQWBD5r1bcMSjuhhmPaV/reH
o6Lq6PATZyMkULthNszBQ6jhCjIm3dxitDIggtRb0I05M6QZDjvsqyrVJEa0fS0T
slmruCBptGLFLkJSDEpG8QdDdcqSmqjw70SZXkIqhIxkroiG+H9kub+ztggJUZU7
QZqvNyfijU/nfGRpOlIp9uK0PFhlrVykDm+67HxlDXae1514ZB4PWUQUW1ucVvdh
Hz03REFKN9XAL4lMAuRGgw21evN3zUxnSVB2GeSytAJ9lQ4DM4kBVAQTAQgAPhYh
BKW4oW2GreyyH7jUiL9I/YT+IdcqBQJZMkL5AhsDBQkDwmcABQsJCAcCBhUICQoL
AgQWAgMBAh4BAheAAAoJEL9I/YT+IdcqCIYH/3p/FvMIhwC52+I0Dwqh0iU9BjaD
Sy9/rlr9HOwtfkqiNN9D/DZxtA83WHXhKqdJ2Oqk7EOwORh/bl8TC4i3Qk0xVz+m
Y8DoY8gESKsFwwXQ/pMSrsPBOYVdaqcjrLbEWFjA89lt/mRd/5VlbLTjEc8qn3GQ
gvOgNII35YWLkO1Yq6fjGhWXkZHY3vkw6nXM5MrZiwSj9aBqmi+pl1pB+6qcOvDi
lD3hNzBuLEWMrPKp8y0WDNexq6HAkN7MW59Xa+qqNpCEpQOC+RvVWw+LzHe+N9mP
W/MWFRRlkgb8RVKq71/U72GT8gD6wuGVIlKH7u060iTN68qHmSG5JtvnRfW5AQ0E
WTJC+QEIAMShMj3yOKAyYjytc+qfqFmwJtVsYBZd3ejarJpNRt0t2Ma90xFXr0vB
mnAV5x7ixIso7cM4ZrVcV/RZZbrtgW14Z81ASdVZYHBdCi+ep9N4Uqfp7nVmRUI9
W6YjQFlpDMLZjtFG+B48qW6i2VCVejWt59wpaIYNikPrWmlC2g96QECCrEKt8AAH
jq0+VLgsK7Dok1aVivBDLM4OBGTwd8G9NHyWtfPbvYBgSOy9f+6KViCPkQK+BfUG
v36b1mSpzvbDT3sUA3RzU1ek+te2HaGt5ewGIqHzLqBOl1yHXCoMCAS6aXFyJfN2
kmFufE5pI6gepBT88qtJYlA6qfWwpYkAEQEAAYkBPAQYAQgAJgIbDBYhBKW4oW2G
reyyH7jUiL9I/YT+IdcqBQJkF+TxBQkOqAj4AAoJEL9I/YT+Idcq5IEH/1K5TGTt
TEVjM4LOakqMD1QMImcGyyyJetGeu6kwhNEaSaJ2LfeMZLVF98XH35+E+WA86N2/
Hbouli6rrMwyso0IZu2iUxhRSYXcfbPu1RbySEeLkKaPhWP/fPIpSfLHDTkPvy+G
l1dOp8XiSqfufABGSbTWTG+e+aZelDNU5GXqNBMENlGVOlgZ4atjKBXQp/N0hRm9
oR9F4P6IXC42hqtaYJBphEYYvOjTCNtLLHOPDpaueXzb4+rWjCiOe/TZ4JnEcx2U
BH9ReQeRtlZvAkDarEXJzyimEBKFkLhAgM4UZjDQJFsUL8eb0ZzPYfNpQgzfEq9t
LcTMf+xqUg8la8k=
=KgHk
-----END PGP PUBLIC KEY BLOCK-----
- valid as of :: <2023-03-20 Mon>
- expiry :: <2025-03-20 Thu>
* Main productions
** [[id:eff4c865-2b7a-4275-a8b1-0fe591553acb][Emacs package]]s
*** [[id:8866e899-d072-41bf-808d-bc8dc115345c][Denote]]
*** [[id:7f67859a-9438-478c-97d9-0e85e9902624][Pulsar]]
** [[id:ba693441-a670-47d1-bad7-91ceecbd5a08][Typeface]]s
*** [[id:9e5f4b99-b1c2-4779-a0b2-6c3d5e4e6620][Iosevka Comfy]]
* References
- [[https://protesilaos.com/][Website]]
- [[https://protesilaos.com/about/]["About me" page]]
- [[https://protesilaos.com/contact/][Contact me | Protesilaos Stavrou]]
- [[https://protesilaos.com/coach/][Coach/tutor on Emacs, Linux, and Life | Protesilaos Stavrou]]
- [[https://sr.ht/~protesilaos/][Main repository]]
  - [[https://gitlab.com/protesilaos/][Gitlab mirror]]
  - [[https://github.com/protesilaos/][Github mirror]]
- [[https://www.youtube.com/@protesilaos/][YouTube channel]]
- [[https://soundcloud.com/protesilaos/][SoundCloud channel]]
- [[https://odysee.com/@protesilaos:6/][Odysee channel]]
