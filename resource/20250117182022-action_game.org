:PROPERTIES:
:ID:       7e810c10-120a-4ae1-9f5a-29f2128d5949
:ROAM_ALIASES: "action games" "action game" "Action Games"
:END:
#+title: Action Game
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2025-01-17 ven 18:20]
#+filetags: :resource:public:

- is :: [[id:7c2a15ee-0a9f-4c73-9151-bf70f553fde1][Video Game]]

* References
- [[https://en.wikipedia.org/wiki/Action_game][Wikipedia]]
