:PROPERTIES:
:ID:       439006b7-d67f-467d-9acb-9c87e06cb745
:ROAM_ALIASES: encodings encoding Encodings
:END:
#+title: Encoding
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-09-08 vr 10:02]
#+filetags: :resource:public:

- relates to :: [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:b9707425-7063-47c6-a426-91d1817ab595][Protocol]]
