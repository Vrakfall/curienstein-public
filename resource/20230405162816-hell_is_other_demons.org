:PROPERTIES:
:ID:       2f37aec7-472c-4dc1-8b25-34b683d87806
:END:
#+title: Hell Is Other Demons
#+filetags: :public:resource:missing-details:
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-05 Wed 16:28]

- is :: [[id:7c2a15ee-0a9f-4c73-9151-bf70f553fde1][Video Game]]
- genre :: [[id:894911c2-e6eb-4d44-931d-111c0b61ba74][Bullet Hell]] [[id:ea375bf9-77a1-4ee2-a44c-e107b81776e3][Shooter]] [[id:db36ce9b-7c47-466c-aca5-3df2b72636e0][Platformer]] [[id:fead8979-f372-4fd3-a9d9-d95cfcc3136e][Arcade]] [[id:de7e8972-2483-4d21-b506-4451f574ed41][Roguelite]]
- developers ::
- publishers ::
- release :: <2019-04-18 Thu>
- platforms :: [[id:f5d42fbb-35de-4b1d-9ce1-adc5c8bd179d][Linux]] [[id:e6fc9b9c-2699-4667-9463-2435c32a327a][Windows]] [[id:0ad4dc50-2e43-4129-b39b-3d13386f1309][MacOS]]
- pacing :: [[id:3f40c957-5b17-4740-96c9-37aa6b14681d][Real-Time]]
- perspective :: [[id:b2bc48f2-011c-413f-bae3-783d0cb60579][Side View]]
- theme :: [[id:cd551812-0fad-4382-a271-3bdda68292f1][Fantasy]]
- modes ::
- engine :: [[id:a8af4755-d950-4a82-9bd0-a6d057a8cbf3][Unity]]
- music :: [[id:c94eefdd-272b-4841-9e4a-561bd2c29fde][The Algorithm]]
- playing status :: [[id:c1080e4b-18ad-4b15-b27f-a9b051c2fc62][To Try]]
- possession status :: [[id:631854be-2fbe-48a3-a043-46b4bb0fbee3][Not Possessed]]
- found from :: [[id:c94eefdd-272b-4841-9e4a-561bd2c29fde][The Algorithm]]'s [[id:22315a3e-5018-4b5d-befa-42c1f8134d65][Discography]]

* References
- [[https://www.hellisotherdemons.com/][Website]]
- [[https://store.steampowered.com/app/595790/Hell_is_Other_Demons/][Steam]]
- [[https://gamicus.fandom.com/wiki/Hell_is_Other_Demons][Codex Gamicus]]
- [[https://www.pcgamingwiki.com/wiki/Hell_is_Other_Demons][PCGamingWiki]]
