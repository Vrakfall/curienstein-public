:PROPERTIES:
:ID:       8841f5bb-df33-426b-a26a-ff54c12e4af2
:ROAM_ALIASES: PDFs .pdf "Portable Document Format"
:END:
#+title: PDF
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-04 Tue 20:30]

- is :: [[id:2e4edbd0-5a2c-427c-b28c-629fb6da32ca][File Format]]
- developer :: [[id:61f182ab-08cf-4074-9304-a4582a191ba6][Adobe]]
- purpose :: [[id:8120741e-4ce5-45b7-95b0-32169c4ef7fa][Document]] [[id:918800fe-1d14-46b1-aeda-ff957fb20a86][Presentation]]

* References
- [[https://en.wikipedia.org/wiki/PDF][Wikipedia]]
