:PROPERTIES:
:ID:       084f33c7-f7ed-4ca8-8e55-9d5f60de8c03
:ROAM_ALIASES: "digital gardens" "digital garden" "Digital Gardens"
:END:
#+title: Digital Garden
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-04 Tue 16:37]

- is :: [[id:b8076d17-99c5-4994-b19a-ee4708f4a70b][Digital]] [[id:808c3e3f-6faa-4f51-b4d8-02552132fc48][Garden]]
- can be :: [[id:2236a32c-94ec-4c72-bf91-bd6b07c2eb88][Zettelkasten]]
