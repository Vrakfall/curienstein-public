:PROPERTIES:
:ID:       486cb21f-2f76-4512-a375-f60eff55c00e
:END:
#+title: Zotero
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-04 Tue 17:02]

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:1d6ae52d-4b95-424b-8e3f-65af1195f8f5][Application]] [[id:6031dd24-3534-4b7d-aa14-7c5176f2229d][Database]] [[id:9ca1eb6c-0cf4-4465-b599-5a42ad38dba3][Open Source]]
- purpose :: [[id:0843b75e-0874-4df7-bb5b-2c8c015d5d6c][Collection]] [[id:9ae265d9-a11e-4416-a823-f8e96ddb448a][Organization]] [[id:ec8579f5-f038-45f0-b911-f812769a4b6d][Annotation]] [[id:8bea10b0-c4b7-4483-8a3e-34f863c5c843][Citation]] [[id:91993314-af53-4ac8-8b18-1eaeb164ddd0][Sharing]] [[id:6e46a055-ec3a-456d-a2da-939de1588594][Research]] [[id:460c5352-a04c-4a55-9eeb-b62040dd3589][Reference Management]]
- license :: [[id:8e3d60e1-bf09-4cc2-bfaf-4a51a2bc2cd7][AGPL v3]]
- language :: [[id:b10f8536-d710-48f4-abf3-831fba795df2][JavaScript]]
- platforms :: [[id:f5d42fbb-35de-4b1d-9ce1-adc5c8bd179d][Linux]] [[id:e6fc9b9c-2699-4667-9463-2435c32a327a][Windows]] [[id:0ad4dc50-2e43-4129-b39b-3d13386f1309][MacOS]] [[id:cdfe85aa-1b22-4d57-9a44-aea424c187a9][iOS]] [[id:bd5488fa-f5af-4cce-95d0-9dbef7797f29][iPadOS]]

* Usage
** Adding Items
- source :: [[https://www.zotero.org/support/adding_items_to_zotero][adding items to zotero [Zotero Documentation]]]
*** From [[id:0b03e9ec-5abb-4b46-b469-f176c9cbb580][Browser]]
**** Install the corresponding [[id:666741e9-bbb6-4d93-8e69-bcc6c1439389][connector]] [[id:5c165c6e-8afc-4514-974d-c989ff433860][extension]]
- Available on the [[https://www.zotero.org/download/][=Downloads=]] page
*** By Identifier
*** [[id:8841f5bb-df33-426b-a26a-ff54c12e4af2][PDF]] and other [[id:e395dc3a-4a77-4f10-8f18-70abf85ab70f][files]]
*** Saving web page
* Plugins
- source :: [[https://www.zotero.org/support/plugins][plugins [Zotero Documentation]]]
* References
- [[https://www.zotero.org/][Website]]
- [[https://github.com/zotero/zotero][Repository]]
- [[https://forums.zotero.org/][Forums]]
- [[https://www.zotero.org/support/][Documentation/Manual]]
  - [[https://www.zotero.org/support/getting_help][Getting Help]]
- [[https://en.wikipedia.org/wiki/Zotero][Wikipedia]]
