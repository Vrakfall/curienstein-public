:PROPERTIES:
:ID:       0017e780-b5e6-467d-9965-de8111539567
:END:
#+title: pot (FreeBSD Jail Manager)
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-30 ven 15:49]
#+filetags: :resource:public:

- is :: [[id:970f1690-2f8a-48b7-8d4a-3cc5bc45e89e][FreeBSD]] [[id:aa16f96e-c28b-4756-a072-a257757a8f74][Jail Manager]]

* References
- [[https://github.com/bsdpot/pot][Repository]]
