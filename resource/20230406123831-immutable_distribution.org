:PROPERTIES:
:ID:       8e60deb2-5711-4a62-ac37-ed06a87b4ff9
:ROAM_ALIASES: "immutable distros" "immutable distro" "Immutable Distros" "Immutable Distro" "immutable distributions" "immutable distribution" "Immutable Distributions"
:END:
#+title: Immutable Distribution
#+filetags: :needs-link-triage:
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-06 Thu 12:38]

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:8d41a5d4-dd88-41ff-8eff-c32ca49bad7e][Operating System]] [[id:6ce0e96b-e553-4f80-b37d-7258897ec10a][Immutable]] [[id:b2473354-9c0e-4543-b4a4-723bd205b6ad][Distribution]]

* Link Inbox
- [[https://itsfoss.com/immutable-linux-distros/][8 Immutable Linux Distributions for Those Looking to Embrace the Future]]
