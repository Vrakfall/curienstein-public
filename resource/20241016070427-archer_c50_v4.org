:PROPERTIES:
:ID:       fb255a49-a3eb-4e64-b381-a86fe091bd9f
:END:
#+title: Archer C50 v4
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-10-16 mer 07:04]
#+filetags: :resource:public:

- is :: [[id:9fa3649c-ca35-424e-b527-b4f5b02fcf70][Router]]
- brand :: [[id:dbe689dd-a89c-46ca-9140-d2ae91f9dd13][TP-Link]]

* [[id:d8986093-9c4f-44d5-93b6-6f43e524f7b0][Hardware Specification]]
- source :: [[https://openwrt.org/toh/tp-link/archer-c50#hardware_highlights][[OpenWrt Wiki] TP-Link Archer C50]]
** Model
Archer C50
** Version
v4
** CPU
MediaTek MT7628A
** CPU MHz
580
** Flash MB
8
** RAM MB
64
** WLAN Hardware
- MediaTek MT7628A
- MediaTek MT7612E
** WLAN 2.4GHz
b/g/n
** WLAN 5.0GHz
a/n/ac
** Ethernet 100M ports
5
** Modem
No
** USB ports
No
* [[id:77aec6e2-4aad-43ec-a6ee-65d6e219246f][OpenWRT]] [[id:bb913a22-2e53-4770-a0b8-04bb9e99814a][Installation]]
* References
- [[https://www.tp-link.com/fr-be/support/download/archer-c50/v4/#Firmware][Firmware Download]]
- [[https://www.tp-link.com/fr-be/home-networking/wifi-router/archer-c50/#specifications][Specifications]]
- [[https://openwrt.org/toh/tp-link/archer-c50][OpenWRT Installation]]
