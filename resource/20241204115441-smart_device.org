:PROPERTIES:
:ID:       fcfc06b9-d69d-4a01-8f84-ec880cc6b6f3
:ROAM_ALIASES: "smart devices" "smart device" "Smart Devices"
:END:
#+title: Smart Device
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-12-04 mer 11:54]
#+filetags: :resource:public:

- is :: [[id:59e818d9-0ba5-47e3-93d1-ef711dce16e3][Appliance]]
- purpose :: [[id:2479147a-4d3c-44ea-97dc-18b207b69796][Automatization]]

* References
- [[https://en.wikipedia.org/wiki/Smart_device][Wikipedia]]
