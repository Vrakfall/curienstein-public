:PROPERTIES:
:ID:       f13d1aa2-e818-476d-9d71-46eff2ded2b7
:ROAM_ALIASES: "function parameters" "function parameter" "Function Parameters"
:END:
#+title: Function Parameter
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-03-19 mar 22:51]
#+filetags: :resource:public:

- is :: [[id:bdbbb4e0-cf89-4090-a668-358cdbc92f5f][Programming Language Idiom]] [[id:41ef4cc4-83ae-4ddb-a87a-63b7c7119eb7][Parameter]]
- relates to :: [[id:d06f60ea-245c-42b7-b909-a24586c8e7b8][Function]] [[id:70a3b408-c4f6-46a7-af76-2db6372c7e79][Programming Language]]
