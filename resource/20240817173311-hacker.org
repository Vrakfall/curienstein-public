:PROPERTIES:
:ID:       842fb083-7b4e-4c00-9a27-c595aeb7b5d0
:ROAM_ALIASES: hackers hacker Hackers
:END:
#+title: Hacker
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-17 Sat 17:33]
#+filetags: :resource:public:

- is :: [[id:54e1ce63-1614-413b-893d-067bd4b16db0][Person]]
- doing :: [[id:2d55e60d-982a-4c5c-a3b5-fe5c59a97197][Hacking]]
