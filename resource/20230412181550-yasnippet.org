:PROPERTIES:
:ID:       7096f884-74a7-4afd-b3a5-ef8f74c26e93
:END:
#+title: YASnippet
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-12 Wed 18:15]
#+filetags: :resource:public:

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:eff4c865-2b7a-4275-a8b1-0fe591553acb][Emacs Package]] [[id:5c165c6e-8afc-4514-974d-c989ff433860][Extension]]
- language :: [[id:aec07f35-d41b-4c04-9bfc-770c290d9c65][Emacs Lisp]]

* Usage
- source :: [[https://joaotavora.github.io/yasnippet/snippet-development.html][Writing snippets]]
** Location
The following variable stores the location of the different directories storing the snippets.
#+begin_src emacs-lisp
yas-snippet-dirs
#+end_src
** Creation
*** Vanilla
#+begin_src emacs-lisp
(yas-new-snippet)
#+end_src
*** [[id:64924874-2950-4ab2-a5a9-1ad1f43897b4][Doom Emacs]]
- default keybind :: =C-c & C-n=
#+begin_src emacs-lisp
(+snippets/new)
#+end_src
* References
- [[https://github.com/joaotavora/yasnippet][Repository]]
- [[https://joaotavora.github.io/yasnippet/][Manual/Documentation]]
