:PROPERTIES:
:ID:       c0bca962-33c4-42b7-84a3-5cf45ba4c3f9
:END:
#+title: tar
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-01 Sat 15:28]

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:c76d6720-6e38-4f5c-8743-784ad8b340e7][CLI]]
- relates to :: [[id:9c510f7f-7593-4abc-b8a1-0f82d7d41faf][Compression]]

* Options
** =-C= OR =--directory=
- tl;dr :: Directory to extract to
