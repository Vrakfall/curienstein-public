:PROPERTIES:
:ID:       db722032-65ea-44df-9813-f148f95b01d3
:ROAM_ALIASES: "Excel sheets" "Excel sheet" "Excel Sheets"
:END:
#+title: Excel Sheet
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-10-23 ma 14:01]
#+filetags: :resource:public:

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:1e03b10c-41fb-4352-b889-5e6d37921911][Excel]] [[id:489a37fd-0aab-49d9-81a2-14edff95d5f0][Calc Sheet]]
