:PROPERTIES:
:ID:       895a39a7-f47d-4c55-a4e4-88159e9b7f2a
:ROAM_ALIASES: stashes Stashes stash
:END:
#+title: Stash
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-29 jeu 02:00]
#+filetags: :resource:public:

- is :: [[id:210f3b7c-61b5-4a44-93cb-e6f7533e80ce][Storage]]
- can be :: [[id:0c1753c8-b94f-47db-97c6-28a2c812eef9][List]]
