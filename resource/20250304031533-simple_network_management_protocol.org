:PROPERTIES:
:ID:       7e6e0493-e2e4-4206-9b06-111c1a0d8408
:ROAM_ALIASES: SNMP
:END:
#+title: Simple Network Management Protocol
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2025-03-04 mar 03:15]
#+filetags: :resource:public:

- is :: [[id:285b8816-d6d4-4cc8-a55a-792bbbe177b3][Network]] [[id:b9707425-7063-47c6-a426-91d1817ab595][Protocol]]
- Layer :: [[id:6297761c-9beb-4cec-84c6-821e83f92921][Layer 7 (OSI)]]

* References
- [[https://en.wikipedia.org/wiki/Simple_Network_Management_Protocol][Wikipedia]]
