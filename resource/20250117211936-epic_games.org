:PROPERTIES:
:ID:       bde26ad6-4116-4d4c-8bc1-fda814a75ced
:END:
#+title: Epic Games
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2025-01-17 ven 21:19]
#+filetags: :resource:public:

- is :: [[id:964094a4-67cf-4ea4-b16d-8a1c5dfd2eeb][Company]] [[id:631b8331-d5cd-412f-bb24-4b603d516ed8][Video Game Developer]] [[id:31081388-3bb6-470c-817b-09f90ce102b6][Video Game Publisher]] [[id:fe78644e-fc4f-4d9d-9479-d5b450f739a7][Software Developer]]

* References
- [[https://en.wikipedia.org/wiki/Epic_Games][Wikipedia]]
- [[https://epicgames.com/][Website]]
