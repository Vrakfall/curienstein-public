:PROPERTIES:
:ID:       9e5f4b99-b1c2-4779-a0b2-6c3d5e4e6620
:ROAM_ALIASES: iosevka-comfy
:END:
#+title: Iosevka Comfy
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-03-24 Fri 19:27]

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:ba693441-a670-47d1-bad7-91ceecbd5a08][Typeface]] [[id:9ca1eb6c-0cf4-4465-b599-5a42ad38dba3][Open Source]]
- author :: [[id:972d2ffc-8c0f-4316-83ff-a354cfbc3f7a][Protesilaos Stavrou]]
- extends :: [[id:1d17e16b-d197-46cf-937c-8eb727276076][IosevkaK]]
- license :: [[id:ccae881f-2fae-4c84-b78c-cb2abd1e40ee][SIL Open Font License]]

* Description
** From the project
#+begin_quote
Customised build of the [[id:1d17e16b-d197-46cf-937c-8eb727276076][Iosevka]] [[id:ba693441-a670-47d1-bad7-91ceecbd5a08][typeface]], with a consistent rounded style and overrides for almost all individual glyphs in both roman (upright) and italic (slanted) variants.
#+end_quote
* References
- [[https://git.sr.ht/~protesilaos/iosevka-comfy/][Repository]]
