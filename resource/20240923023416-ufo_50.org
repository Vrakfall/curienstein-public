:PROPERTIES:
:ID:       57067add-a397-4351-b1e6-b58fddbf1ac9
:END:
#+title: UFO 50
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-09-23 lun 02:34]
#+filetags: :resource:public:

- is :: [[id:7c2a15ee-0a9f-4c73-9151-bf70f553fde1][Video Game]] [[id:0843b75e-0874-4df7-bb5b-2c8c015d5d6c][Collection]] [[id:e9632538-6b26-44ef-8773-018574660479][Interesting]] [[id:096e56fa-7f53-4930-a844-bd37d126918c][To Be Discovered]]

* References
- [[https://50games.fun/][Website]]
