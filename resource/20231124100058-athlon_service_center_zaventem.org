:PROPERTIES:
:ID:       eef45410-77d3-48dc-858f-ca72ae8847db
:END:
#+title: Athlon Service Center (Zaventem)
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-11-24 vr 10:00]
#+filetags: :resource:public:

- is :: [[id:1b4d60b2-7667-4cb7-99f6-467d8fefe689][Athlon]] [[id:76856dbf-a8fa-4817-8fd0-713532393dff][Mechanic Shop]]
- location :: [[id:021ff05a-00c0-4b23-bd64-67855bcbb2cd][Zaventem]]
