:PROPERTIES:
:ID:       76856dbf-a8fa-4817-8fd0-713532393dff
:ROAM_ALIASES: "mechanic shops" "mechanic shop" "Mechanic Shops"
:END:
#+title: Mechanic Shop
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-11-24 vr 10:03]
#+filetags: :resource:public:

- is :: [[id:9c689a70-f053-4acd-a165-17560738e86e][Mechanical]] [[id:6ca76e04-3ff4-4221-84c7-6b84a476b529][Shop]]
