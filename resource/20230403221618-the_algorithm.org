:PROPERTIES:
:ID:       c94eefdd-272b-4841-9e4a-561bd2c29fde
:END:
#+title: The Algorithm
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-03 Mon 22:16]

- does :: [[id:cf7eb970-ee3f-4d11-8607-602574c41d5c][Music]]
- genres :: [[id:5e7fb965-e976-47f2-b97b-14e5fd6b96e3][Electronic]] [[id:8397a346-e738-4247-ad42-78d101c08579][IDM]] [[id:92510c23-134a-4b16-96c5-4d05cc427010][Progressive Metal]]
- status :: [[id:c1080e4b-18ad-4b15-b27f-a9b051c2fc62][To Listen]]
- main license :: [[id:33353e98-6dd5-49dd-9d3e-55e16874810e][Copyright]]

* References
- [[https://www.thealgorithmmusic.com/][Website]]
- [[https://thealgorithm.bandcamp.com/][Bandcamp]]
- [[https://en.wikipedia.org/wiki/The_Algorithm][Wikipedia]]
