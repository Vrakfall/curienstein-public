:PROPERTIES:
:ID:       35dbb5b6-ec50-42ce-802a-5e7d464b6107
:END:
#+title: Dirvish
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-09 ven 04:32]
#+filetags: :resource:public:

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:eff4c865-2b7a-4275-a8b1-0fe591553acb][Emacs Package]]
- extends :: [[id:1e6bf6c7-2048-43d0-986f-22c84d008e6c][Emacs]] [[id:916e4b2a-ee1d-444b-ba26-ed45285433f5][Dired]]

* Description
* [[id:5c165c6e-8afc-4514-974d-c989ff433860][Extensions]]
- [[https://github.com/alexluigit/dirvish/blob/main/docs/EXTENSIONS.org][dirvish/docs/EXTENSIONS.org at main · alexluigit/dirvish · GitHub]]
* Known issues
- It is abandonned for a year (as of August 2024) and doesn't work well at the moment with [[id:64924874-2950-4ab2-a5a9-1ad1f43897b4][Doom Emacs]] but the author of the latter is working on a new fork to merge it at some point.
* References
- [[https://github.com/alexluigit/dirvish][Repository]]
