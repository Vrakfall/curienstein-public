:PROPERTIES:
:ID:       2f4b585c-31ad-4eec-b499-23ef2cd3f8a8
:END:
#+title: Waydroid
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-10-03 jeu 00:17]
#+filetags: :resource:public:

- is :: [[id:1d6ae52d-4b95-424b-8e3f-65af1195f8f5][Application]] [[id:9e2d8bf9-a751-475a-bfc9-c6cae5e97a3a][Container]] [[id:2479147a-4d3c-44ea-97dc-18b207b69796][Automatization]]
- purpose :: [[id:8365bc5e-2050-4624-8df2-9da8698deb97][Android]] [[id:1d6ae52d-4b95-424b-8e3f-65af1195f8f5][Application]] [[id:a48d6267-81a3-4952-b3dc-1813a190f7b4][Virtualization]]

* Description
* Installation
** Pre-requisite
[[id:06871ecb-2d63-4ea0-b44f-6ec9ea37c5dd][Specific]] [[id:5d8cbdd3-e9c8-481d-b9a1-a314f47592ed][CPU]] architectures, check the [[id:ced01dde-1481-4791-8643-db2a09960078][ArchWiki]] page.
** [[id:bbf78978-13bf-40a2-967c-2883325de8c2][Linux Kernel]] [[id:01bca411-89e0-483c-89fe-fcc7860add31][patching]] methods
There are multiple methods to install it, like [[id:f951198f-b3f3-44bb-8a8e-bb4fe61e301f][building]] [[id:06871ecb-2d63-4ea0-b44f-6ec9ea37c5dd][specific]] [[id:bbf78978-13bf-40a2-967c-2883325de8c2][Linux Kernels]].
Only the tried ones are listed here.
*** [[id:ebca8518-3f4a-441a-9c0a-7bb11e270a88][Dynamic Kernel Module Support]]
- [[id:bb913a22-2e53-4770-a0b8-04bb9e99814a][Install]] [[https://aur.archlinux.org/packages/binder_linux-dkms][binder_linux-dkms (AUR)]]
- [[id:f9ef2b46-4e55-42ab-bc2d-2bc76eaa3e1b][Load]] [[id:9a3dd1a8-fc97-492c-bf24-950c407b3d67][Linux Kernel Modules]] ~binder_linux~ with =options= ~devices=binder,hwbinder,vndbinder~
  #+begin_src shell
modprobe binder-linux device=binder,hwbinder,vndbinder
  #+end_src
- On [[id:c8bd4313-e2da-4167-a837-f5c0557112df][Intel]] [[id:5d8cbdd3-e9c8-481d-b9a1-a314f47592ed][CPUs]] only
  - Set [[id:e4d4bb2c-cee2-4d43-a3ce-c3dcb579159a][Linux Kernel Parameter]] ~kernel.ibt=off~
** Rest of [[id:bb913a22-2e53-4770-a0b8-04bb9e99814a][Install]]
- [[id:bb913a22-2e53-4770-a0b8-04bb9e99814a][Install]] [[https://aur.archlinux.org/packages/waydroid][waydroid (AUR)]]
- Initialization (Download the latest [[id:8365bc5e-2050-4624-8df2-9da8698deb97][Android]] [[id:c4e575d3-54f0-41d7-bf99-9138614b10b2][image]])
  #+begin_src shell
waydroid init
# OR To include Google Apps support:
waydroid init -s GAPPS
  #+end_src
  - reference :: [[id:44c7fce2-80e6-4265-8ee5-3ae26ffdd46d][Google]] [[id:64fb41c4-3e35-4251-b678-e9836ff7df00][Google Apps]]
  - needs :: [[id:8651304c-375b-4cec-a500-4d0603f8da70][Root Access]]
- Start or enable ~waydroid-container.service~'s [[id:24c58a0e-63dd-45c5-acf6-c2e4245a2ac9][systemd service]]
* Usage
** Launch a [[id:d6671bff-3cb2-4b3c-a640-28316f1b7e3f][session]]
- The [[id:b700e282-0dfa-46f0-8d4a-c998dc65ae05][service]] has to be started for this step.
- The other [[id:89e95cfe-f8bf-4a1b-bb38-ad3335d25a9e][commands]] have to be executed after this session is started.
  #+begin_src shell
waydroid session start
  #+end_src
** Launch a [[id:9a19384d-e1d5-4bf4-a11e-b6fc10ba2e93][GUI]]
#+begin_src shell
waydroid show-full-ui
#+end_src
** Launch a [[id:885f79de-4b0f-457b-9d44-c1ddf6a4463f][shell]]
#+begin_src shell
waydroid shell
#+end_src
** [[id:bb913a22-2e53-4770-a0b8-04bb9e99814a][Install]] [[id:1d6ae52d-4b95-424b-8e3f-65af1195f8f5][application]]
#+begin_src shell
waydroid app install "$path/to/apk"
#+end_src
** Get the [[id:1d6ae52d-4b95-424b-8e3f-65af1195f8f5][application]] [[id:0c1753c8-b94f-47db-97c6-28a2c812eef9][list]]
#+begin_src shell
waydroid app list
#+end_src
** Run an [[id:1d6ae52d-4b95-424b-8e3f-65af1195f8f5][application]]
#+begin_src shell
waydroid app launch $package_name
#+end_src
* References
- [[https://waydro.id/][Website]]
- [[https://github.com/waydroid/waydroid][Repository]]
- [[https://wiki.archlinux.org/title/Waydroid][ArchWiki]]
- [[https://docs.waydro.id/][Documentation]]
- [[https://matrix.to/#/#waydroid:matrix.org][Matrix]]
- [[https://t.me/WayDroid][Telegram]]
