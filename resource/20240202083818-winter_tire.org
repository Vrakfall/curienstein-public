:PROPERTIES:
:ID:       55a0fd7f-651f-4cec-b847-1be224578486
:ROAM_ALIASES: "Winter tires" "Winter tire" "Winter Tires"
:END:
#+title: Winter Tire
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-02-02 vr 08:38]
#+filetags: :resource:public:

- is :: [[id:7cdfa770-b47e-4005-a872-b351a8e97f79][Tire]]
