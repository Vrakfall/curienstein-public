:PROPERTIES:
:ID:       7efcefc9-536a-4701-86fd-2f76ae1ee6da
:ROAM_ALIASES: cargo
:END:
#+title: Cargo
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-02-25 dim 18:43]
#+filetags: :resource:public:

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:e83e79b4-1072-441c-96ba-d6ec3a7e1f8e][Library Manager]]
- library manager for :: [[id:b576f9b8-54d5-4157-958a-7d05328045f7][Rust]] [[id:f3b43b2d-21a2-4334-a78e-46fa2c5506bd][Rust Crates]]

* Extensions
** [[id:f4e85d26-74b8-49e7-907a-030e5f4b9922][cargo-outdated]]
** [[id:bcff2898-f859-461b-8cff-9c1d2a2b0b77][cargo-edit]]
* References
- [[https://doc.rust-lang.org/cargo/commands/cargo-run.html][cargo run - The Cargo Book]]
