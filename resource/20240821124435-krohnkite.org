:PROPERTIES:
:ID:       fa9423a8-8f21-48c3-8c0d-ede3177adc1e
:END:
#+title: Kröhnkite
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-21 Wed 12:44]
#+filetags: :resource:public:

- is :: [[id:f1e24230-82bd-4fd1-8cc3-570bac46fdc2][KWin Script]] [[id:84bc1114-94b1-4fbd-bef5-ce5623ba77be][Tiling Window Manager]]

* References
- [[https://github.com/anametologin/krohnkite][Repository]]
- [[https://store.kde.org/p/2144146][KDE Store page]]
