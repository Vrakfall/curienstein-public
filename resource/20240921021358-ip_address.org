:PROPERTIES:
:ID:       a40ff0c0-2efe-48c5-a64f-b26d657d572b
:ROAM_ALIASES: IPs IP "IP addresses" "IP address" "IP Addresses"
:END:
#+title: IP Address
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-09-21 sam 02:13]
#+filetags: :resource:public:

- is :: [[id:285b8816-d6d4-4cc8-a55a-792bbbe177b3][Network]] [[id:d93f26d7-a731-4040-b6c3-c22b507b55d0][Layer 3 (OSI)]]
