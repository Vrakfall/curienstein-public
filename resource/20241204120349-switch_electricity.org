:PROPERTIES:
:ID:       0da385ad-c3eb-4fe6-afd5-0f5f635ff1d1
:ROAM_ALIASES: "switches (Electricity)" "switch (Electricity)" "Switches (Electricity)"
:END:
#+title: Switch (Electricity)
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-12-04 mer 12:03]
#+filetags: :resource:public:

- is :: [[id:2c2080e3-da68-4031-81c7-ff29f4c32a55][Electrical]] [[id:59e818d9-0ba5-47e3-93d1-ef711dce16e3][Device]]

* References
- [[https://en.wikipedia.org/wiki/Switch][Wikipedia]]
