:PROPERTIES:
:ID:       d8a557b9-75c7-495e-a9f5-c1b4df639153
:END:
#+title: Fossil
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-10-08 mar 19:07]
#+filetags: :resource:public:

- is :: [[id:76693d00-d18a-4623-98f2-3ae0c37bc757][Version Control System]]

* Description
- [[https://materialious.nadeko.net/watch/Zn_JTg3JDbI][Fossil: Github in a box. | Materialious]]
** Main [[id:0123774b-fbd5-4fcd-af47-86bbc9682173][Hubs]]
- [[id:2dee5647-84ad-4b80-a217-d806bdb55219][Chisel]]
* [[id:21497c61-1d4c-4874-a186-71a114c4c6cd][Code Editor]] [[id:5c165c6e-8afc-4514-974d-c989ff433860][Extensions]]
** [[id:1e6bf6c7-2048-43d0-986f-22c84d008e6c][Emacs]]
There are multiple old [[id:eff4c865-2b7a-4275-a8b1-0fe591553acb][Emacs Packages]] available but [[id:4c355478-a80a-448d-8a4a-40eea768cc07][emacs-fossil]] seems the best without trying.
- [[id:4c355478-a80a-448d-8a4a-40eea768cc07][emacs-fossil]]
*** [[id:0013322f-e64f-40cf-9856-46b4df8b3970][Exploration]]
- [[https://fossil-users.fossil-scm.narkive.com/OfBZdvqp/fossil-and-emacs][[fossil-users] Fossil and Emacs]]
- [[https://fossil-scm.org/forum/forumpost/8228c6bc7df76eedadd7ef017dab2a28e394de827c57f7d255129e9375928979][Fossil Forum: vc-mode for Emacs and fossil]]
* Usage
- source :: [[https://www.fossil-scm.org/home/doc/trunk/www/quickstart.wiki][Fossil: Fossil Quick Start Guide]]
** Compared to [[id:859520d6-d0b0-4763-8f47-35f732bd3741][Git]]
- source :: [[https://fossil-scm.org/home/doc/trunk/www/gitusers.md][Fossil: Git to Fossil Translation Guide]]
** Check-ins
- source :: [[https://www.fossil-scm.org/home/doc/trunk/www/checkin_names.wiki][Fossil: Check-in Names]]
** Diffs
*** Colorized Diffs
- source :: [[https://fossil-scm.org/home/doc/trunk/www/colordiff.md][Fossil: Colorized Diffs]]
* [[id:635a1c86-40de-43c3-b03b-1e241d33aef9][Configuration]]
Most [[id:635a1c86-40de-43c3-b03b-1e241d33aef9][configurations]] of [[id:d8a557b9-75c7-495e-a9f5-c1b4df639153][Fossil]] are done via the local [[id:094f8165-14a7-49a7-9059-9cb51e30b4ed][Web Server]] included with it.
** [[id:6c2c9225-bc47-4a35-a503-479e7ed01a4f][Skin]]
*** Potential [[id:6c2c9225-bc47-4a35-a503-479e7ed01a4f][Skin]] [[id:0843b75e-0874-4df7-bb5b-2c8c015d5d6c][collections]]
- [[https://fossil.include-once.org/fossil-skins/index][Fossil Skins Extra: Fossil Skins Extra]]
- [[https://fossil.wanderinghorse.net/r/fossil-skins/wiki/fossil-skins][fossil-skins: fossil-skins]]
* [[id:87ca9ec0-6c9a-40f6-a553-e93c9476d556][Server]]
** Headless
*** ~fossil server~
- is :: [[id:89e95cfe-f8bf-4a1b-bb38-ad3335d25a9e][Command]]
- documentation :: [[https://www.fossil-scm.org/home/help/server][Fossil: Help: server]]
** Local
To start the local [[id:094f8165-14a7-49a7-9059-9cb51e30b4ed][web server]] and directly open its home page:
#+begin_src shell
fossil ui [repository-filename]
#+end_src
The file is not mandatory when ran within a checked out [[id:fd018f62-f4c0-4569-ae1c-701925ec8151][directory]].
* References
- [[https://www.fossil-scm.org/][Website]]
- [[https://www.fossil-scm.org/home/doc/trunk/www/permutedindex.html][Documentation]]
- [[https://www.mail-archive.com/fossil-users@lists.fossil-scm.org/][Mailing List]]
- [[https://www2.fossil-scm.org/home/doc/trunk/www/faq.wiki#q8][FAQ]]
** [[id:25ab027a-9b65-4392-b2ff-fb48541c57d2][Articles]]
- [[https://www.linux-magazine.com/Issues/2016/186/Workspace-Fossil-SCM][Fossilized Code » Linux Magazine]]
