:PROPERTIES:
:ID:       b50e68bc-ee99-4330-8944-ca67f789550d
:END:
#+title: Power Automate
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-03-07 do 17:32]
#+filetags: :resource:public:

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]]
- from :: [[id:000ff836-6f7a-4f96-985f-d5a1c9e4fca8][Microsoft]]
