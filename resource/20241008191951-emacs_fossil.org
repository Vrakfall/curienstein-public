:PROPERTIES:
:ID:       4c355478-a80a-448d-8a4a-40eea768cc07
:END:
#+title: emacs-fossil
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-10-08 mar 19:19]
#+filetags: :resource:public:

- is :: [[id:d8a557b9-75c7-495e-a9f5-c1b4df639153][Fossil]] [[id:eff4c865-2b7a-4275-a8b1-0fe591553acb][Emacs Package]]

* Description
* References
- [[https://github.com/venks1/emacs-fossil][Repository]]
- [[https://www.emacswiki.org/emacs/fossil][EmacsWiki]]
