:PROPERTIES:
:ID:       7e5d72b6-13e6-41d9-91ff-083914dbf89f
:ROAM_REFS: https://www.orgroam.com/
:ROAM_ALIASES: org-roam
:END:
#+title: Org-Roam
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-03-09 Thu 21:35]

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:9ca1eb6c-0cf4-4465-b599-5a42ad38dba3][Open Source]] [[id:eff4c865-2b7a-4275-a8b1-0fe591553acb][Emacs package]] [[id:f1191815-df14-4342-8cb1-24ba8bb72382][GNU]]
- usage :: [[id:11cfc7e1-cd62-4ee6-afa7-b24e29b3ebb5][Personal Knowledge Management]] [[id:9ae265d9-a11e-4416-a823-f8e96ddb448a][Organization]]
- extends :: [[id:1e6bf6c7-2048-43d0-986f-22c84d008e6c][Emacs]] [[id:eb8a18b3-ec4f-4086-a7ae-c4ca35c3aab8][Org-Mode]]
- languages :: [[id:aec07f35-d41b-4c04-9bfc-770c290d9c65][Emacs Lisp]]
- license :: [[id:4609ac5c-69e9-43c8-bbab-0c3d4c1f39f1][GPL v3]]
- author :: [[id:a5dcd53d-576d-421f-a77a-be8ddd0ee6ab][Jethro Kuan]]

* Description
** From me
Org-Roam is an extension to [[id:eb8a18b3-ec4f-4086-a7ae-c4ca35c3aab8][Org-Mode]] that allows to manage =org= files in a [[id:2236a32c-94ec-4c72-bf91-bd6b07c2eb88][Zettelkasten]] manner. This is useful to build a personal knowledge base (like this one).
It is an [[id:1e6bf6c7-2048-43d0-986f-22c84d008e6c][Emacs]]' extension.
** From Website
#+begin_quote
A plain-text personal knowledge management system.
#+end_quote
* [[id:01f168d3-1563-44e9-b8a1-0391ef6177ea][Templates]]
** Links
- [[https://www.orgroam.com/manual.html#The-Templating-System][Manual]]
- [[https://systemcrafters.net/build-a-second-brain-in-emacs/capturing-notes-efficiently/][Capturing Notes Efficiently with Org Roam - System Crafters]]
** Preserving newlines
*** Easiest solution
- As per [[help:org-roam-capture-templates][org-roam-capture-templates]]'s [[id:25e9e3b1-1c16-4601-82f7-2c302580c314][code]] [[id:e95553b7-d4a2-413d-8ddc-58d4f5c84b75][documentation]], each template can make use of the following properties to better handle newlines upon capture:
  - =:empty-lines= :: Number of lines that should be inserted before and after the new item
  - =:empty-lines-before= :: Number of lines that should be inserted before the new item
  - =:empty-lines-after= :: Number of lines that should be inserted after the new item
*** More details and other solutions
- [[https://org-roam.discourse.group/t/multiple-blank-lines-in-head-of-capture-template/2488][Here's a thread from a user wondering about it.]]
- It used to [[https://github.com/org-roam/org-roam/blob/main/org-roam-capture.el#L776-L791][clean every newline]] in both the [[id:01f168d3-1563-44e9-b8a1-0391ef6177ea][Template]] head and body
  - [[https://github.com/org-roam/org-roam/pull/2117/files][This commit]] should make them stay for either the body or the head (but I think it applies to the body) when =ENSURE-NEWLINE= is set but I've been unable to find out how to do that.
  - Here's [[https://github.com/org-roam/org-roam/issues/1368][an even older issue]] where [[id:a5dcd53d-576d-421f-a77a-be8ddd0ee6ab][Jethro Kuan]] said he wouldn't fix it because it is due to [[id:eb8a18b3-ec4f-4086-a7ae-c4ca35c3aab8][Org-Mode]]'s capture expansion mechanisms.
* Unlinked References
- [[https://old.reddit.com/r/emacs/comments/sjstjx/how_can_i_get_orgroam_unlinked_references_working/][How can I get org-roam unlinked references working? : emacs]]
- [[https://org-roam.discourse.group/t/finding-unlinked-references-org-roam-sbl-show-broken-links/878/7][Finding Unlinked References (org-roam-sbl-show-broken-links) - Development - Org-roam]]
* [[id:71a7544e-347e-474b-9ddb-d7eabb20e84e][Performance]]
** [[id:60e3d7b2-ae12-458d-bdb1-9c2a49936af8][Discussions]] on the [[id:f107b2d2-097c-4392-a319-aa892fc1d2ec][issue]]
- [[https://org-roam.discourse.group/t/materialized-view-for-org-roam-db-using-sqlite-triggers/3483][Materialized View for Org-roam.db using sqlite triggers - Development - Org-roam]]
- [[https://org-roam.discourse.group/t/improving-org-roam-capture-and-templates/3372][Improving org-roam capture and templates - Development - Org-roam]]
- [[https://org-roam.discourse.group/t/preconceived-ideas-org-roam-node-find-performance/3568][Preconceived Ideas & org-roam-node-find Performance - Development / UX & UI - Org-roam]]
- [[https://org-roam.discourse.group/t/rewriting-org-roam-node-list-for-speed-it-is-not-sqlite/3475][Rewriting org-roam-node-list for speed (it is not sqlite) - Development - Org-roam]]
- [[https://org-roam.discourse.group/t/improving-performance-of-node-find-et-al/3326][Improving performance of node-find et. al - Development - Org-roam]]
** [[id:ba5c6977-113a-44e8-b4ad-b48423e8054c][Packages]] trying to solve it
- [[https://github.com/dmgerman/org-roam-gt][GitHub - dmgerman/org-roam-gt: Faster finding of nodes and better templates]]
- [[id:e9ac1a50-2305-48c7-b88f-29439830e84c][Vulpea]]
- A third one I need to find back
* References
- [[https://www.orgroam.com/][Website]]
- [[https://www.orgroam.com/manual.html][Documentation/Manual]]
- [[https://github.com/org-roam/org-roam/][Repository]]
** Articles about [[id:7e5d72b6-13e6-41d9-91ff-083914dbf89f][Org-Roam]]
- [[https://takeonrules.com/2022/02/07/org-roam-emacs-and-ever-refining-the-note-taking-process/][Org Roam, Emacs, and Ever Refining the Note Taking Process // Take on Rules]]
