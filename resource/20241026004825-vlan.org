:PROPERTIES:
:ID:       a37beb85-0437-4518-967b-ee4f0d246f32
:ROAM_ALIASES: vlans vlan VLANs
:END:
#+title: VLAN
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-10-26 sam 00:48]
#+filetags: :resource:public:

- is :: [[id:22475513-9a18-4779-b945-e8724d3e1819][Layer 2 (OSI)]]
- relates to :: [[id:285b8816-d6d4-4cc8-a55a-792bbbe177b3][Network]]

* 802.1Q
- source :: [[https://en.wikipedia.org/wiki/IEEE_802.1Q][IEEE 802.1Q - Wikipedia]]
The main [[id:f802081a-ec05-419a-8dbb-2c572eb1cc5a][implementation]] nowadays
** Range
From 0 to 4095
** Reserved
- 0
  - [[id:65753794-7ea7-4f69-afb8-b9182e6fde35][Native]] [[id:a37beb85-0437-4518-967b-ee4f0d246f32][VLAN]]
  - Untagged, the PCP/DEI tag indicates a priority
- 1
  - Sometimes taken by some [[id:160ed614-4079-4f5e-8b04-88f3f2f9345e][switches]] for [[id:285b8816-d6d4-4cc8-a55a-792bbbe177b3][network]] [[id:ead4860f-19aa-4042-a03a-0d1f0dd81043][management]]
- 4095
  - [[id:f802081a-ec05-419a-8dbb-2c572eb1cc5a][Implementation]] use
  - Can be used to indicate a wildcard match
*** Other reserved [[id:a37beb85-0437-4518-967b-ee4f0d246f32][VLANs]]
[[id:06871ecb-2d63-4ea0-b44f-6ec9ea37c5dd][Specific]] [[id:448e32d6-4fe8-4bef-a9f3-0a9ad53614f4][Network Appliance]] brands can reserve some [[id:a37beb85-0437-4518-967b-ee4f0d246f32][VLANs]] for various reasons, i.e. [[id:db1fa2f6-783c-40f6-aa80-816054beff97][backward compatibility]].
**** [[id:372529c4-0d13-4d96-9805-f430d7d0efb6][Cisco IOS]]
- [[id:64a19aec-31c7-4862-871c-19d650a1b1b7][Reserved VLANs]]
* References
- [[https://en.wikipedia.org/wiki/VLAN][Wikipedia]]
