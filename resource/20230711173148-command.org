:PROPERTIES:
:ID:       89e95cfe-f8bf-4a1b-bb38-ad3335d25a9e
:ROAM_ALIASES: commands command Commands
:END:
#+title: Command
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-07-11 di 17:31]
#+filetags: :resource:public:

- is :: [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:885f79de-4b0f-457b-9d44-c1ddf6a4463f][Shell]] [[id:9350d7cb-f0b8-43cb-bbe1-8a1e25a50872][Program]]
