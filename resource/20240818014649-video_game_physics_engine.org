:PROPERTIES:
:ID:       52b8c2c2-29f8-42b9-a62e-4d143f1e775f
:ROAM_ALIASES: "physics engines" "physics engine" "Physics Engines" "Physics Engine" "video game physics engines" "video game physics engine" "Video Game Physics Engines"
:END:
#+title: Video Game Physics Engine
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-18 dim 01:46]
#+filetags: :resource:public:

- is :: [[id:d4aacfb8-1438-4646-a655-c9c0d36d96ed][Video Game Physics]] [[id:bc94e537-403a-4c38-b558-2424c5f2b1b1][Engine]]
