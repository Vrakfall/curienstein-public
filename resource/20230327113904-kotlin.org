:PROPERTIES:
:ID:       52911b92-f7a4-4696-ba64-58308eaab31c
:END:
#+title: Kotlin
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-03-27 Mon 11:39]

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:70a3b408-c4f6-46a7-af76-2db6372c7e79][Programming Language]] [[id:9ca1eb6c-0cf4-4465-b599-5a42ad38dba3][Open Source]]
- author :: [[id:ce7ab3b1-b02b-4e45-bbe2-42274491d2bb][JetBrains]]
- license :: Multiple
- based on :: [[id:2845996c-ab26-4c25-a154-7e1dd7351b99][Java]]
- interoperates with :: [[id:2845996c-ab26-4c25-a154-7e1dd7351b99][Java]]
- runs on :: [[id:5a0265c0-75ad-43ea-bfd4-1a25093afc4a][JVM]]

* Description
** From me
[[id:52911b92-f7a4-4696-ba64-58308eaab31c][Kotlin]] is a [[id:8e870133-6f3b-4d14-a31c-b4979acaec30][High-Level]] [[id:70a3b408-c4f6-46a7-af76-2db6372c7e79][Programming Language]] built on top of [[id:2845996c-ab26-4c25-a154-7e1dd7351b99][Java]].
Its [[id:e2a96116-b2ec-4c88-a88d-a11444018d6e][type inference]] capabilities allows it to have a more concise [[id:ab3b0391-5a78-45d6-9058-0f6e527f1467][syntax]] that of [[id:2845996c-ab26-4c25-a154-7e1dd7351b99][Java]].
* References
- [[https://kotlinlang.org/][Website]]
- [[https://github.com/JetBrains/kotlin/][Repository]]
- [[https://en.wikipedia.org/wiki/Kotlin_(programming_language)/][Wikipedia]]
