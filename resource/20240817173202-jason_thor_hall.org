:PROPERTIES:
:ID:       901b6d9d-9d3e-4795-a3de-b9fd4c894369
:END:
#+title: Jason "Thor" Hall
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-17 Sat 17:32]
#+filetags: :resource:public:

- is :: [[id:54e1ce63-1614-413b-893d-067bd4b16db0][Person]] [[id:1914e6cd-5d49-44e2-ace6-89f28d563af6][Streamer]] [[id:842fb083-7b4e-4c00-9a27-c595aeb7b5d0][Hacker]]
- CEO of/created/works for :: [[id:3d4f2efa-0d91-465a-a95a-868762c832c7][Pirate Software]]
- position :: [[id:0078b407-fcd3-4a7d-b0a1-0789c6117fb4][CEO]] [[id:6b4373ea-4311-4e4e-9030-4b8d6ca0043c][Programmer]] [[id:2dd22dd5-c7ff-40ba-b237-ad8b89291d84][Video Game Designer]]
