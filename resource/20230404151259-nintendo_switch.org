:PROPERTIES:
:ID:       7caec973-97bc-4b90-bbb5-bad907feb190
:END:
#+title: Nintendo Switch
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-04 Tue 15:12]

- is :: [[id:7e02a391-e099-4bb3-b607-acfdb41f2183][Game Console]]
- developer :: [[id:b672b384-63ba-4ee5-b154-890f48de9869][Nintendo]]

* References
- [[https://en.wikipedia.org/wiki/Nintendo_Switch][Wikipedia]]
