:PROPERTIES:
:ID:       c92f5faf-1664-476c-b7a0-259f4fd6fd60
:END:
#+title: Vilvoorde
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-08-10 do 16:17]
#+filetags: :missing-details:resource:public:

- is :: [[id:ed6a361d-9f16-4e5e-aca2-020a994b162f][Town]]
- location :: [[id:858c8987-3af9-4dd3-b801-1fa633d00612][Belgium]]
