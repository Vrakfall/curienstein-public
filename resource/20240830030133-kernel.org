:PROPERTIES:
:ID:       be0ab119-1786-4958-932f-458352fa7121
:ROAM_ALIASES: kernels kernel Kernels
:END:
#+title: Kernel
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-30 Fri 03:01]
#+filetags: :resource:public:

- relates to :: [[id:8d41a5d4-dd88-41ff-8eff-c32ca49bad7e][Operating System]]
