:PROPERTIES:
:ID:       a734f474-0fe0-4800-baab-3750e2d8c9eb
:END:
#+title: Equans
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-05-05 Fri 10:31]
#+filetags: :public:missing-details:resource:

- is :: [[id:964094a4-67cf-4ea4-b16d-8a1c5dfd2eeb][Company]]
- industry :: [[id:2c2080e3-da68-4031-81c7-ff29f4c32a55][Electrical]] [[id:c6775b20-9c61-49af-97ff-b8be5601afc0][HVAC]] [[id:f48c2f74-1167-40d9-9de9-bf94e98c0ce8][Cooling Protection]] [[id:9d711079-86aa-446b-bba8-b59b893fabcf][Fire Protection]] [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:7052ed29-406f-44f8-845f-f5e06aa954b2][Facility Management]] [[id:9c689a70-f053-4acd-a165-17560738e86e][Mechanicals]] [[id:7ef73164-31b5-46de-91f2-ca3e093254d0][Robotics]] [[id:d874ad6f-096d-4798-93d2-c363da4aadd7][Nuclear]] [[id:5565fae5-fff0-41ce-856b-75795748d9ab][Energy]] [[id:464a6595-cc20-42bd-bccd-0d6cc0c10525][Biotechnology]] [[id:1ac82cc7-13d1-4b16-9506-7d690b714c87][Pharmaceuticals]] Others
- belongs to :: [[id:31833fb6-e80b-49d2-b2d8-89a8f7e1fca9][Bouygues]]
- location :: [[id:6927f3e2-1d89-47f6-b2e7-d7701f2e5a1a][Brussel]] [[id:eeb55ec0-a861-4333-8b96-95c9bae31041][Houthalen]]
- subcontractor of :: [[id:12e39821-fa19-4206-9f44-d30133e0acf8][Proximus]]

* History
** Merges
In [[id:c067f1a8-73c9-46ef-83fa-8d3d9996b022][2021]], [[id:31833fb6-e80b-49d2-b2d8-89a8f7e1fca9][Bouygues]] acquired [[id:a734f474-0fe0-4800-baab-3750e2d8c9eb][Equans]] from [[id:55f60e40-4bcf-4381-aa1c-bdc1ab65f2ba][Engie]].
* Ethos
- source :: [[https://www.equans.com/about-us/ethics-compliance][Ethics & Compliance | Equans Group]]
- Acting in accordance with [[id:223611cd-e668-42d3-817b-d09c7aa4c42a][laws]] and [[id:e81db3b0-bf4d-482e-b112-6388ab215749][regulations]]
- Behaving honestly and promote a culture of [[id:9b4a3bea-5d5b-4d4b-af0e-3b2061820000][integrity]]
- Be [[id:416278cc-aa73-437b-a0e7-0c33d749b87f][Loyal]]
- [[id:186d6939-af6f-405c-8333-5639cf4cd076][Respect]]
* Identity
- source :: [[https://www.equans.com/about-us/identity-card][Identity card | Equans Group]]
- [[id:be54d57f-c77c-4ea6-8964-48ea8b8f27b8][Energy Transition]]
- [[id:19f25c92-9b0e-4595-ac0d-da6819a25609][Digital Transition]]
- [[id:ddc7270f-effe-4e5d-95b9-5a3e22d71ee8][Industrial Transition]]
* References
- [[https://www.equans.com/][Website]]
