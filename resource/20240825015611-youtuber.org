:PROPERTIES:
:ID:       bf7b47a8-04e8-403b-8e9d-7d1765412571
:ROAM_ALIASES: youtuber
:END:
#+title: Youtuber
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-25 dim 01:56]
#+filetags: :resource:public:

- is :: [[id:54e1ce63-1614-413b-893d-067bd4b16db0][Person]]
- on :: [[id:c03481f0-b5de-470b-95b2-635cd3dd0988][Youtube]]
