:PROPERTIES:
:ID:       6e1968fa-4fe6-43ac-b055-80de3fb76100
:ROAM_ALIASES: "game development"
:END:
#+title: Game Development
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-05 Wed 10:53]

- is :: [[id:bd9bf76b-9d33-4b81-a5aa-8db0d3d3c5b9][Development]]
- develops :: [[id:d4448bd8-b6b9-4b43-862f-925c9309a78b][Games]]

* Points to take into account
** From [[id:b35c922f-c06b-434f-8d49-4d909b5de0e8][Asmongold]]'s [[id:a2c584f6-827f-449c-8462-9d816917d8e6][video]]
- [[id:47b1428a-7b9e-4a90-8d54-9464d29aaf0e][Game Play]] should always come before [[id:3e565b9c-299e-4e03-b2b9-a378ffca78bd][Lore]]
  - [[id:ef60ad80-d32a-41b4-890b-b7929cf6d5aa][Players]] will always try to [[id:f864c7e9-4051-49ea-8d31-12fa06103c60][Min-Max]] everything that is available to them
    - This can easily turn [[id:d2c1cd1d-c766-4e68-a444-c1f87a5dcfca][Features]] into [[id:c6b26344-96fe-4c37-b088-2b380daf5deb][Anti-Features]]
    - This fact should be known, embraced and [[id:6b77374b-ce00-4553-92b4-3854c47c0c59][planned]] around.
- Multiple [[id:85452717-1a5b-42b8-9676-246703c72f03][factions]] usually don't matter and usually only split-up the [[id:4df807c6-63c9-4861-be2b-96e7712caa17][playerbase]], turning them into [[id:c6b26344-96fe-4c37-b088-2b380daf5deb][anti-features]]
  - Has to be considered if [[id:85452717-1a5b-42b8-9676-246703c72f03][factions]] are needed
*** TODO Link to it
** TODO Clean this
