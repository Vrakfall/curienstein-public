:PROPERTIES:
:ID:       9cdd29d4-ef08-4b8c-8ef1-e710dd476bdd
:END:
#+title: AUR
#+filetags: :repository:package:community:third-party:code:software:it:open-source:
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-03-16 Thu 17:50]

* Description
** From me
The [[id:9cdd29d4-ef08-4b8c-8ef1-e710dd476bdd][AUR]] is a user repository for additional [[id:22aca5b4-0a81-4891-8ab4-fada43fc480f][ArchLinux]] packages to be shared by the community.
* .SRCINFO
#+begin_quote
.SRCINFO files contain package metadata in a simple, unambiguous format, so that tools such as the AUR's Web back-end or AUR helpers may retrieve a package's metadata without parsing the PKGBUILD directly.
#+end_quote
- [[https://wiki.archlinux.org/title/.SRCINFO][ArchWiki page]]
* References
- [[https://aur.archlinux.org/][Website]]
- [[https://wiki.archlinux.org/title/AUR_submission_guidelines][AUR submission guidelines]]
