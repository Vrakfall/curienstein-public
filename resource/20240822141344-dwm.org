:PROPERTIES:
:ID:       da5a43e2-cf28-4c5d-ac30-858479c0a147
:END:
#+title: dwm
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-22 jeu 14:13]
#+filetags: :resource:public:

- is :: [[id:84bc1114-94b1-4fbd-bef5-ce5623ba77be][Tiling Window Manager]]

* Usage
- [[https://dwm.suckless.org/tutorial/][dwm - dynamic window manager | suckless.org software that sucks less]]
- [[https://www.ratfactor.com/dwm][Dave's Visual Guide to dwm - ratfactor]]
* References
- [[https://dwm.suckless.org/][Website]]
