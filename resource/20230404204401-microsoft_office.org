:PROPERTIES:
:ID:       ccb722c4-a85f-4cfb-82da-02fb69373854
:END:
#+title: Microsoft Office
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-04 Tue 20:44]

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:dcd43817-e630-4e43-bd97-62ce421c33ec][Productivity Software]]
- developer :: [[id:000ff836-6f7a-4f96-985f-d5a1c9e4fca8][Microsoft]]

* References
- [[https://en.wikipedia.org/wiki/Microsoft_Office][Wikipedia]]
