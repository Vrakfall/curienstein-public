:PROPERTIES:
:ID:       ee9b7526-10b8-44fb-aa75-ab0f010dd784
:END:
#+title: Jade
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-06 Thu 12:46]

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:bb913a22-2e53-4770-a0b8-04bb9e99814a][Installer]]
- used for :: [[id:0771cbf7-7cc7-442f-aaa8-b0d57d1a1d0b][Crystal Linux]]

* References
- [[https://git.getcryst.al/crystal/software/jade][Repository]]
