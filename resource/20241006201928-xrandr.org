:PROPERTIES:
:ID:       3e0aba01-4403-4ebf-a12f-3f13ed16dafb
:END:
#+title: xrandr
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-10-06 dim 20:19]
#+filetags: :resource:public:

- is :: [[id:8d078d92-e03e-4335-94e2-18610c0ef9e6][Xorg]] [[id:89e95cfe-f8bf-4a1b-bb38-ad3335d25a9e][Command]]

* Description
* Usage
** Separate scaling per [[id:c0d04a53-69b5-448a-9e66-3625758dbe19][monitor]]
:PROPERTIES:
:ID:       f186c261-7a28-4f2e-b761-620af08e1261
:END:
#+begin_src shell
xrandr --output "$display_name" --scale 0.8x0.8
# OR
xrandr --output "$display_name" --scale-from 1920x1080
#+end_src
* References
