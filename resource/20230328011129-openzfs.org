:PROPERTIES:
:ID:       3d134ace-8820-43b2-9a78-77fe67301b9f
:ROAM_ALIASES: "ZFS on Linux" zfs ZFS ZoL
:END:
#+title: OpenZFS
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-03-28 Tue 01:11]

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:42e1d403-832a-4607-84ec-8556b7eb950b][Filesystem]] [[id:e37d468d-9198-4cda-89e3-24e655701cc4][Copy-on-write]]
- features :: [[id:ceb52ad8-4e27-4e73-b28a-c2cc788e9dee][Snapshots]]

* Usage
** [[id:89e95cfe-f8bf-4a1b-bb38-ad3335d25a9e][Command]] [[id:c52b82e4-ebd3-476c-be12-565394120906][Cheatsheets]]
- [[https://itsjustbytes.wordpress.com/2020/05/10/zfs-command-line-reference-cheat-sheet/][ZFS command line reference (Cheat sheet) – It’s Just Bytes…]]
** Pool
| Action | [[id:89e95cfe-f8bf-4a1b-bb38-ad3335d25a9e][Command]]                         |
|--------+---------------------------------|
| Create | =zpool create datapool device0= |
** Mounting
*** Recursively
- source :: [[https://serverfault.com/questions/450818/recursively-mounting-zfs-filesystems][freebsd - recursively mounting ZFS filesystems? - Server Fault]]
#+begin_src shell
zfs list -rH -o name pool/path/dir | xargs -L 1 zfs mount
#+end_src
** [[id:b064c0f7-1601-4278-8ba4-2dcd3f96980f][Backup]]
*** [[id:b064c0f7-1601-4278-8ba4-2dcd3f96980f][Backup]] [[id:ceb52ad8-4e27-4e73-b28a-c2cc788e9dee][snapshot]] to a [[id:6c6ec435-e82e-4818-9b8c-a5589f7e73b3][zstd]] archive
For a full [[id:b064c0f7-1601-4278-8ba4-2dcd3f96980f][backup]] (not incremental):
#+begin_src shell
zfs send -wR pool/dataset@snapshot | zstd -19 -o /path/to/file.zst -T16
#+end_src
*** Restore from a [[id:6c6ec435-e82e-4818-9b8c-a5589f7e73b3][zstd]] archive
#+begin_src shell

#+end_src
*** Other [[id:54e1ce63-1614-413b-893d-067bd4b16db0][people]]'s [[id:b064c0f7-1601-4278-8ba4-2dcd3f96980f][backup]] [[id:eba94a7a-0612-41fc-b41e-cc1e3f1ccf09][strategies]]
- [[https://www.kassner.com.br/en/2020/12/19/zfs-backup-strategy/][My ZFS backup strategy | Rafael Kassner]]
* [[id:bdf80f70-f880-4af3-b3cc-45c94c08af8d][Forums]]
- [[https://discourse.practicalzfs.com/][Practical ZFS]]
* References
- [[https://openzfs.org/][Website]]
- [[https://github.com/openzfs/zfs][Repository]]
