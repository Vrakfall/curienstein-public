:PROPERTIES:
:ID:       2afc81f0-3a05-4f72-b06d-4958356653a7
:ROAM_ALIASES: RiverMakes Shaye
:END:
#+title: Bradie "Shaye" Rehmel
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-17 Sat 17:40]
#+filetags: :resource:public:

- is :: [[id:54e1ce63-1614-413b-893d-067bd4b16db0][Person]] [[id:92b83711-ecc4-47f0-92f4-8239cac671f8][Artist]]
- works for :: [[id:3d4f2efa-0d91-465a-a95a-868762c832c7][Pirate Software]]
- position :: [[id:85e6c0b4-a625-49f7-b8df-77608fbc2121][Lead Artist]]
- twitch name :: =RiverMakes=
