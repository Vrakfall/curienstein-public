:PROPERTIES:
:ID:       c9e8e09e-7c58-4743-97f8-2da55ae38094
:END:
#+title: Berchem
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-09-01 vr 15:16]
#+filetags: :resource:public:

- is :: [[id:ed6a361d-9f16-4e5e-aca2-020a994b162f][Town]]
- location :: [[id:858c8987-3af9-4dd3-b801-1fa633d00612][Belgium]]
  - near :: [[id:4b899678-22f3-4e3a-b427-e976ccbc9ef2][Antwerpen]]
