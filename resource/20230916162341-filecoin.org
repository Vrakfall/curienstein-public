:PROPERTIES:
:ID:       7110b53e-c953-45cf-99ea-564be86940ac
:END:
#+title: Filecoin
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-09-16 sam 16:23]
#+filetags: :resource:public:

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:b66abce3-5d67-4ea7-b337-84bcbde7ccb2][Cryptocurrency]] [[id:0d971cf3-0779-4686-ae1f-e32a29cc98eb][Peer-to-Peer]] [[id:3750b533-99c0-43fa-8a20-64178fa5e7c0][File Storage]] [[id:66aaa116-d50f-435c-9253-836f93770684][File Synchronisation]]
- leveraging :: [[id:06f707a0-80be-40d9-9c4d-a3be89a7d3bf][IPFS]]

* Description
* References
- [[https://filecoin.io/][Webiste]]
- [[https://docs.filecoin.io/][Documentation]]
