:PROPERTIES:
:ID:       8beb673b-589b-47dd-b317-1c4cb4bb2dcc
:ROAM_ALIASES: "server racks" "server rack" "Server Racks"
:END:
#+title: Server Rack
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-07-17 ma 12:15]
#+filetags: :resource:public:

- is :: [[id:59e818d9-0ba5-47e3-93d1-ef711dce16e3][Hardware]]
- relates to :: [[id:87ca9ec0-6c9a-40f6-a553-e93c9476d556][Server]]
