:PROPERTIES:
:ID:       bcfd0188-e43f-4388-bc78-536f767684aa
:ROAM_ALIASES: storytelling
:END:
#+title: Storytelling
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-05 Wed 11:03]

- relates to :: [[id:a77facb9-2316-4ffc-8e05-705a5c45c3fd][Writing]] [[id:80e9fbe5-e9bf-48b5-8d36-0764e889f7e0][Story]]

* Point to take into account
** From that [[id:b35c922f-c06b-434f-8d49-4d909b5de0e8][Asmongold]]'s [[id:a2c584f6-827f-449c-8462-9d816917d8e6][video]]
- Do not hesitate to add [[id:2f9075f2-5cdf-4723-802e-7f58d87673d0][grey lines]] and [[id:f3b9b5db-7041-469f-ad65-ad83cb66554b][characters]] with grey [[id:6df0e6c7-03e9-4978-8cea-30869546eec3][values]]
  - Too often, [[id:7c2a15ee-0a9f-4c73-9151-bf70f553fde1][Video Game]] [[id:3e565b9c-299e-4e03-b2b9-a378ffca78bd][Lores]] are just filled up with [[id:071a5166-ee53-48d1-9b52-3ec925cddc0e][manichaeism]]
*** TODO Link to it
