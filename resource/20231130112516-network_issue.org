:PROPERTIES:
:ID:       54923dc9-9f42-43fb-9ab3-e00ee595ce5f
:ROAM_ALIASES: "network issues" "network issue" "Network Issues"
:END:
#+title: Network Issue
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-11-30 do 11:25]
#+filetags: :resource:public:

- is :: [[id:285b8816-d6d4-4cc8-a55a-792bbbe177b3][Network]] [[id:f107b2d2-097c-4392-a319-aa892fc1d2ec][Issue]]
