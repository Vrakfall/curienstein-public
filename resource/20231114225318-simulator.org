:PROPERTIES:
:ID:       2a0aba8f-7235-44a7-9edb-58156f446927
:ROAM_ALIASES: simulators simulator Simulators
:END:
#+title: Simulator
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-11-14 mar 22:53]
#+filetags: :resource:public:

- is :: [[id:b780d6f2-beec-4f60-8ba7-f8a72eaa5c55][Simulation]] [[id:59e818d9-0ba5-47e3-93d1-ef711dce16e3][Hardware]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]]
