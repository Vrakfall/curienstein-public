:PROPERTIES:
:ID:       40197ddb-6dd3-4198-b9e3-0094890b3836
:ROAM_ALIASES: "GPL licenses"
:END:
#+title: GPL Licenses
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-09-21 sam 05:43]
#+filetags: :resource:public:

- is :: [[id:605e3a59-e544-4297-ba50-57aa2e30c466][Copyleft]] [[id:0a57021a-d408-4f2e-897e-8bb670276a72][Open Source License]] [[id:caacf785-1c9b-4a90-bfa0-7d81e395ce66][Family]]
