:PROPERTIES:
:ID:       31552e65-9c8e-4d04-8146-8136e7333821
:ROAM_ALIASES: burnouts burnout Burnouts
:END:
#+title: Burnout
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-30 Fri 13:05]
#+filetags: :resource:public:

- relates to :: [[id:77bee8d6-f603-4d0a-aca4-6d6b8116187d][Psychology]]
