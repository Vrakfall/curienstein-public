:PROPERTIES:
:ID:       91c7a0a4-476c-4be3-9932-9e8d9b9e5419
:END:
#+title: KDE
#+filetags: :public:resource:kde:environment:desktop:code:open-source:software:community:it:
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-03-14 Tue 17:29]

* Description
** From Wikipedia
#+begin_quote
[[id:91c7a0a4-476c-4be3-9932-9e8d9b9e5419][KDE]] is an international [[https://en.wikipedia.org/wiki/Free_software_movement][free software community]] that develops [[https://en.wikipedia.org/wiki/Free_and_open-source_software][free and open-source software]].
#+end_quote
* Main projects
** [[id:5a82a832-c832-4f00-9cba-21c685d5507f][Plasma]]
*** [[id:18d3cec7-6384-4297-9766-e49308b00569][KWin]]
* References
- [[https://kde.org/][Website]]
- [[https://en.wikipedia.org/wiki/KDE][Wikipedia]]
- [[https://develop.kde.org/docs/][Documentation]]
- [[https://invent.kde.org/explore/groups?sort=name_asc][Gitlab]]
