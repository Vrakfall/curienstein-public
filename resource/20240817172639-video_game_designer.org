:PROPERTIES:
:ID:       2dd22dd5-c7ff-40ba-b237-ad8b89291d84
:ROAM_ALIASES: "game designers" "game designer" "Game Designers" "Game Designer" "video game designers" "video game designer" "Video Game Designers"
:END:
#+title: Video Game Designer
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-17 Sat 17:26]
#+filetags: :resource:public:

- is :: [[id:9de37f76-489f-4b64-8f53-0a379b42504c][Company Role]]
- doing :: [[id:7db93b2f-26a9-48d6-a348-760c067b68ac][Video Game Design]]
