:PROPERTIES:
:ID:       456606a3-98a1-4819-9fcb-9f4834eaca69
:ROAM_ALIASES: quickemu
:END:
#+title: Quickemu
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-10-11 ven 18:05]
#+filetags: :resource:public:

- is :: [[id:42428978-c49d-44c6-81ee-0375cfbee8c1][QEMU]] [[id:d84cabb0-1943-4ba6-89e1-89a7fba13150][Wrapper]]

* Usage
** [[id:bb913a22-2e53-4770-a0b8-04bb9e99814a][Install]] [[id:0ad4dc50-2e43-4129-b39b-3d13386f1309][MacOS]] in a [[id:f558630f-e822-4a52-8dc6-a65bbead199c][Virtual Machine]]
- [[https://itsfoss.com/macos-linux-vm/][I Installed macOS on Linux in a VM (for fun sake)]]
* References
- [[https://github.com/quickemu-project/quickemu?tab=readme-ov-file][Repository]]
