:PROPERTIES:
:ID:       e395c0c5-412d-402b-9a0e-c45cf6d19cba
:ROAM_ALIASES: "heuristic search algorithms" "heuristic search algorithm" "Heuristic Search Algorithms"
:END:
#+title: Heuristic Search Algorithm
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-01-26 ven 18:37]
#+filetags: :resource:public:

- is :: [[id:cfe6395c-8519-426b-9e11-e3255f2f0bdc][Heuristic]] [[id:b83a05c6-2e65-42b1-ab13-3eb69ca9d46d][Search Algorithm]]
