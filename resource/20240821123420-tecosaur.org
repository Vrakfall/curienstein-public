:PROPERTIES:
:ID:       c2fcd394-e74e-4297-ae6a-679126cf69e5
:END:
#+title: tecosaur
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-08-21 Wed 12:34]
#+filetags: :resource:public:

- is :: [[id:54e1ce63-1614-413b-893d-067bd4b16db0][Person]] [[id:9ca1eb6c-0cf4-4465-b599-5a42ad38dba3][Open Source]] [[id:0aaab563-259c-4b78-971e-dc5874230600][Developer]]

* [[id:64924874-2950-4ab2-a5a9-1ad1f43897b4][Doom Emacs]]
- [[https://tecosaur.github.io/emacs-config/config.html][Doom Emacs Configuration]]
* References
- [[https://github.com/tecosaur][Github profile]]
