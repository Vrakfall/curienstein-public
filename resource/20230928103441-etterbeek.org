:PROPERTIES:
:ID:       72554a45-2dce-4995-bfe5-023c9ec22860
:END:
#+title: Etterbeek
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-09-28 do 10:34]
#+filetags: :resource:public:

- is :: [[id:ed6a361d-9f16-4e5e-aca2-020a994b162f][Town]] [[id:9f893448-e46d-4cbf-8ee7-c00503021a4a][City]]
- location :: [[id:858c8987-3af9-4dd3-b801-1fa633d00612][Belgium]]
