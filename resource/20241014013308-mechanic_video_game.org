:PROPERTIES:
:ID:       11b2fc0b-3089-4a59-98e6-d25130759a9c
:ROAM_ALIASES: mechanic Mechanic "mechanics (video game)" "mechanic (video game)" "Mechanics (Video Game)"
:END:
#+title: Mechanic (Video Game)
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-10-14 lun 01:33]
#+filetags: :resource:public:

- relates to :: [[id:7c2a15ee-0a9f-4c73-9151-bf70f553fde1][Video Game]]

* References
