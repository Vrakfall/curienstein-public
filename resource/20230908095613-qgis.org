:PROPERTIES:
:ID:       611a5832-58db-4ab9-821b-ef2d5dd313de
:END:
#+title: QGIS
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-09-08 vr 09:56]
#+filetags: :missing-details:resource:public:

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]]

* Usage
** Export to [[id:ddfffb25-ca0f-48b7-8fec-ec52b14856d0][AutoCAD]]
[[id:7058b553-d9e8-4aad-94a0-cd7f65a604b7][Export]] may fail due to the [[id:439006b7-d67f-467d-9acb-9c87e06cb745][encoding]] used, a correct one is =cp1254=.
