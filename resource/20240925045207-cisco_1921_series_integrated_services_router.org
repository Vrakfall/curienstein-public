:PROPERTIES:
:ID:       be73ef1a-c855-402c-a130-a285bb6ef0ea
:ROAM_ALIASES: c1921 "Cisco 1921"
:END:
#+title: Cisco 1921 Series Integrated Services Router
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-09-25 mer 04:52]
#+filetags: :resource:public:

- is :: [[id:d897957f-b93a-4732-8240-3af3de082a47][Cisco Router]]

* Notice
Most of the information about this [[id:9fa3649c-ca35-424e-b527-b4f5b02fcf70][router]] is in [[id:0b3d2623-dec9-4a8c-8682-cb752404c324][Cisco 1900 Series Integrated Services Router]] and [[id:372529c4-0d13-4d96-9805-f430d7d0efb6][Cisco IOS]].
* Description
* [[id:cf75bbaf-1bd2-45de-ab4e-ce8d254eff46][Specifications]]
- source :: [[https://itprice.com/cisco/cisco1921-sec/k9.html][CISCO1921-SEC/K9 Price Datasheet Cisco 1900 Series Security Bundles]]
** Dimensions & Weight
*** Width
13.5 in
*** Depth
11.5 in
*** Height
1.7 in
*** Weight
11.9 lbs
** Environmental Parameters
*** Min Operating Temperature
32 °F
*** Max Operating Temperature
104 °F
*** Humidity Range Operating
10 - 85%
** Expansion Slots
*** Type
=EHWIC=
** Flash [[id:1697847c-d573-4c80-b5d0-546a7c319563][Memory]]
*** Type
=Flash=
*** Installed Size
256 MB
*** Max Supported Size
256 MB
** Header
*** Compatibility
=PC=
*** Manufacturer
[[id:59bdf795-fa4b-4071-8a66-c2a2f0d5fd1e][Cisco]]
*** Packaged Quantity
1
*** Product Line
[[id:59bdf795-fa4b-4071-8a66-c2a2f0d5fd1e][Cisco]]
*** Model
1921
*** Brand
[[id:59bdf795-fa4b-4071-8a66-c2a2f0d5fd1e][Cisco]]
** Interface Provided
*** Type
=USB 2.0=
*** Connector Type
4 pin USB Type A
*** Interface
Auxiliary
*** Qty
1
** Miscellaneous
*** Rack Mounting Kit
Optional
*** Compliant Standards
- AS/NZS 3548 Class A
- AS/NZS 60950-1
- BSMI CNS 13438
- CAN/CSA-E60065-00
- CISPR 22 Class A
- CISPR 24
- CS-03
- CSA C22.2 No. 60950-1
- EN 60950-1
- EN 61000-3-2
- EN300-386
- EN50082-1
- EN55022 Class A
- EN55024
- FCC CFR47 Part 15
- ICES-003 Class A
- IEC 60950-1
- UL 60950-1
- VCCI
*** Height (Rack Units)
1
** Networking
*** Type
Router
*** Connectivity Technology
Wired
*** Data Link Protocol
Ethernet, Fast Ethernet, Gigabit Ethernet
*** Remote Management Protocol
- RMON
- SNMP
*** Compliant Standards
- IEEE 802.1ag
- IEEE 802.1ah
- IEEE 802.1Q
- IEEE 802.3ah
*** Features
- Class-Based Weighted Fair Queuing (CBWFQ)
- Firewall protection
- IPv6 support
- MPLS support
- NetFlow
- Quality of Service (QoS)
- Syslog support
- TR-069 Agent
- VLAN support
- VPN support
- Web Services Management Agent (WSMA)
- Weighted Random Early Detection (WRED)
*** Network Transport Protocol
IPSec, L2TPv3
*** Routing Protocol
- BGP
- DVMRP
- EIGRP
- GRE
- IGMPv3
- IS-IS
- MPLS
- OSPF
- PIM-SM
- PIM-SSM
- Policy-based routing (PBR)
- Static IP routing
- Static IPv4 routing
- Static IPv6 routing
*** Modular
Modular
*** Form Factor
- Desktop
- Rack-mountable
*** Key Features
- Firewall
- USB port
- VPN support
** OS Provided
*** Type
[[id:372529c4-0d13-4d96-9805-f430d7d0efb6][Cisco IOS]] Security
** Power Device
*** Frequency Required
50/60 Hz
*** Power Consumption Operational
60 Watt
*** Nominal Voltage
AC 120/230 V
*** Type
Internal power supply
** RAM
*** Installed Size
512 MB
*** Max Supported Size
512 MB
** Service
*** Support Details Type
Limited warranty
*** Support Details Service Included
Advance parts replacement
*** Support Details Full Contract Period
1 year
** Service & Support
*** Type
- 1 year limited warranty
- 1 year warranty
** Service & Support Details
*** Response Time
10 days
** Slot Provided
*** Total Qty
2
*** Free Qty
2
* [[id:28c483e0-2660-4e59-b749-59dc72c5a495][Cisco License]] activation
:PROPERTIES:
:ID:       2a2aceb7-3096-4432-83b9-e82afbfe9085
:END:
- I got a clue in [[https://community.cisco.com/t5/other-security-subjects/securityk9-license-4331-eval-to-righttouse-to-permanent-extra/td-p/4172946][this forum post]] that could be a lot easier than going through all the hoops of the [[id:59bdf795-fa4b-4071-8a66-c2a2f0d5fd1e][Cisco]] [[id:7f8407c5-35d5-4e79-8788-5ab423d7859e][websites]] and pay for a [[id:28c483e0-2660-4e59-b749-59dc72c5a495][Cisco License]].
- This [[https://old.reddit.com/r/Cisco/comments/w2k62x/how_to_enable_securityk9_license_in_c1900_packet/][Reddit thread]] showed me how to enable the license.
  #+begin_src ios-config
R1>en
R1#configure terminal
R1(config)#license boot module c1900 technology-package securityk9
R1(config)#
  #+end_src
  - This probably works because mine were previously permanently enabled by the [[id:964094a4-67cf-4ea4-b16d-8a1c5dfd2eeb][company]] I bought them from.
* [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]]
** Download [[id:0f175791-8cfb-4c61-84c1-0387f42323b1][locations]]
- [[https://software.cisco.com/download/home/282977114/type][Software Download - Cisco Systems]]
* References
- [[https://www.cisco.com/c/en/us/products/collateral/routers/1900-series-integrated-services-routers-isr/data_sheet_c78-598389.html][Data Sheet]]
