:PROPERTIES:
:ID:       ccae881f-2fae-4c84-b78c-cb2abd1e40ee
:ROAM_ALIASES: OFL
:END:
#+title: SIL Open Font License
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-03-24 Fri 19:38]

- is :: [[id:605e3a59-e544-4297-ba50-57aa2e30c466][Copyleft]] [[id:0a57021a-d408-4f2e-897e-8bb670276a72][Open Source License]]
- applies to :: [[id:ba693441-a670-47d1-bad7-91ceecbd5a08][Font]] and font-related [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]]

* References
- [[https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL][Page]]
- [[https://en.wikipedia.org/wiki/SIL_Open_Font_License][Wikipedia]]
