:PROPERTIES:
:ID:       13504c8f-470d-4f5e-a88f-6e6c9d129a26
:ROAM_ALIASES: HLS
:END:
#+title: HTTP Live Streaming
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-09-04 Wed 00:32]
#+filetags: :resource:public:

- is :: [[id:c8527ed5-a64a-4131-bbaa-642ce3c1edc4][Streaming]] [[id:b9707425-7063-47c6-a426-91d1817ab595][Protocol]]
- based on :: [[id:0c57701b-ebe4-4890-9a83-5808d279bd63][HTTP]]

* References
- [[https://en.wikipedia.org/wiki/HTTP_Live_Streaming][Wikipedia]]
