:PROPERTIES:
:ID:       7c2a15ee-0a9f-4c73-9151-bf70f553fde1
:ROAM_ALIASES: "video games" "video game" "Video Games"
:END:
#+title: Video Game
#+filetags: :public:resource:
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-03 Mon 19:26]

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:d4448bd8-b6b9-4b43-862f-925c9309a78b][Game]] [[id:09ccd5ab-5714-481d-b010-e202f5ab98a8][Interactive]]

* Recommendation tools
- Quantic Foundry: [[https://apps.quanticfoundry.com/recommendations/gamerprofile/videogame/][Video Game Recommendation Engine]]
* To check
- [[https://www.igdb.com/games/hardspace-shipbreaker][Hardspace: Shipbreaker (2022)]]
- [[https://illarion-ev.itch.io/illarion][Illarion by Illarion e.V., Brightrim]]
  - [[https://github.com/Illarion-eV/Illarion-Server][GitHub - Illarion-eV/Illarion-Server: Server for the online RPG Illarion]]
