:PROPERTIES:
:ID:       f103b4fa-2434-4b2e-8b6c-42a7e69fb013
:ROAM_ALIASES: spa-json
:END:
#+title: SPA-JSON
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-09-10 Tue 02:42]
#+filetags: :resource:public:

- is :: [[id:2ba665d7-bfdc-4869-a178-6abb0128c80f][Data Format]]
- supersedes :: [[id:c81b5298-de81-47ac-a38d-7b87a5aecb05][JSON]]
