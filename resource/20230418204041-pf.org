:PROPERTIES:
:ID:       7e10deb1-904d-40e8-9bf2-d78217272266
:ROAM_ALIASES: "Packet Filter"
:END:
#+title: pf
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-04-18 Tue 20:40]
#+filetags: :resource:public:

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:8ab60f5f-c8b9-4cad-919e-7d6f5e3fd814][Firewall]]

* References
- [[https://en.wikipedia.org/wiki/PF_(firewall)][Wikipedia]]
