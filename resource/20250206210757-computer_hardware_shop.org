:PROPERTIES:
:ID:       8ce786cc-9ac9-4666-9612-89f140f03941
:ROAM_ALIASES: "computer hardware shops" "computer hardware shop" "Computer Hardware Shops"
:END:
#+title: Computer Hardware Shop
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2025-02-06 jeu 21:07]
#+filetags: :resource:public:

- is :: [[id:6b83f734-292d-4019-8d59-f85e2e16796e][Computer]] [[id:59e818d9-0ba5-47e3-93d1-ef711dce16e3][Hardware]] [[id:6ca76e04-3ff4-4221-84c7-6b84a476b529][Shop]]

* References
