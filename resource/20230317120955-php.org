:PROPERTIES:
:ID:       674b41b5-15ed-4ada-a087-552cc7fa761d
:END:
#+title: PHP
#+filetags: :public:resource:
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-03-17 Fri 12:09]

* Description
** From [[https://en.wikipedia.org/wiki/PHP][Wikipedia]]
#+begin_quote
*PHP* is a [[https://en.wikipedia.org/wiki/General-purpose_programming_language][general-purpose]] [[https://en.wikipedia.org/wiki/Scripting_language][scripting language]] geared toward [[https://en.wikipedia.org/wiki/Web_development][web development]].^{[[https://en.wikipedia.org/wiki/PHP#cite_note-8][(8)]]} It was originally created by Danish-Canadian [[https://en.wikipedia.org/wiki/Programmer][programmer]] [[https://en.wikipedia.org/wiki/Rasmus_Lerdorf][Rasmus Lerdorf]] in 1993 and released in 1995.^{[[https://en.wikipedia.org/wiki/PHP#cite_note-:0-9][(9)]]}^{[[https://en.wikipedia.org/wiki/PHP#cite_note-10][(10)]]} The PHP [[https://en.wikipedia.org/wiki/Reference_implementation][reference implementation]] is now produced by The PHP Group.^{[[https://en.wikipedia.org/wiki/PHP#cite_note-about_PHP-11][(11)]]} PHP was originally an abbreviation of /*Personal Home Page*/ ,^{[[https://en.wikipedia.org/wiki/PHP#cite_note-History_of_PHP-12][(12)]]}^{[[https://en.wikipedia.org/wiki/PHP#cite_note-13][(13)]]} but it now stands for the [[https://en.wikipedia.org/wiki/Recursive_initialism][recursive initialism]] /*PHP: Hypertext Preprocessor*/ .^{[[https://en.wikipedia.org/wiki/PHP#cite_note-14][(14)]]}
#+end_quote
* References
- [[https://en.wikipedia.org/wiki/PHP][Wikipedia]]
