:PROPERTIES:
:ID:       4eac097b-e093-4300-85fa-36ed3dbbc199
:END:
#+title: Valve
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2025-01-17 ven 21:27]
#+filetags: :resource:public:

- is :: [[id:964094a4-67cf-4ea4-b16d-8a1c5dfd2eeb][Company]] [[id:631b8331-d5cd-412f-bb24-4b603d516ed8][Video Game Developer]] [[id:31081388-3bb6-470c-817b-09f90ce102b6][Video Game Publisher]] [[id:fe78644e-fc4f-4d9d-9479-d5b450f739a7][Software Developer]] [[id:a4405bfa-b766-485e-8f4a-91cf24c2a459][Digital Distribution]]

* References
- [[https://en.wikipedia.org/wiki/Valve_Corporation][Wikipedia]]
- [[https://www.valvesoftware.com/en/][Website]]
