:PROPERTIES:
:ID:       9c2ee22b-f6ae-4b85-9466-9edb4d35d882
:ROAM_ALIASES: "function overload"
:END:
#+title: Function Overload
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2024-03-23 Sat 17:51]
#+filetags: :resource:public:

- is :: [[id:8e5cf9fd-2c56-476c-a873-810797d02a85][IT]] [[id:40af0fb6-55e9-42f1-959b-3a53ad16abf5][Software]] [[id:bdbbb4e0-cf89-4090-a668-358cdbc92f5f][Programming Language Idiom]]
- relates to :: [[id:d06f60ea-245c-42b7-b909-a24586c8e7b8][Function]]

* References
- [[https://en.wikipedia.org/wiki/Function_overloading][Wikipedia]]
* Description
Ability to create multiple [[id:d06f60ea-245c-42b7-b909-a24586c8e7b8][functions]] of the same name with different [[id:f802081a-ec05-419a-8dbb-2c572eb1cc5a][implementations]]
