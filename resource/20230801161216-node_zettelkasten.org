:PROPERTIES:
:ID:       fe07c53e-9652-4513-87c8-81dd3f503e21
:ROAM_ALIASES: zettels zettel Zettels Zettel "nodes (Zettelkasten)" "node (Zettelkasten)" "Nodes (Zettelkasten)"
:END:
#+title: Node (Zettelkasten)
#+author: Jérémy "Vrakfall" Lecocq
#+date: [2023-08-01 di 16:12]
#+filetags: :resource:public:

- part of :: [[id:2236a32c-94ec-4c72-bf91-bd6b07c2eb88][Zettelkasten]]
